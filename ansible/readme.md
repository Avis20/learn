[TOC]
# ansible [ключи] -i [инвентарь] -m [модуль] [хост|группа хостов|] #


# Links #

https://pypi.python.org/pypi/ansible/2.3.0.0
[Список модулей для ansible](http://docs.ansible.com/ansible/list_of_all_modules.html)

---

# Установка #

```
#!bash

sudo apt-get install ansible

```

# Ключи #

---
* -i 
    *  Указывает какой "инвертарь" использовать.
Пример:  

hosts
```
#!bash

host0.example.org ansible_host=192.168.33.10 ansible_user=root

```
где,  
ansible_ssh_host - переменная, которая содержит IP-адрес узла, к которому будет создаваться соединение  
ansible_ssh_user - переменная которая говорит Ansible'у подключаться под указанным аккаунтом (юзером). По умолчанию Ansible использует ваш текущий аккаунт, или другое значение по умолчанию, указанное в ~/.ansible.cfg (remote_user).  

* -m
    * Указывает какой модуль использовать(см. раздел Модули)
Пример:
```
#!bash

ansible -m ping all -i hosts

```

---

# Модули #

** Эти же модули можно использовать в плейбуках **

## shell ##

** Запуск shell-команд на удаленном узле **

Пример:  
```
#!bash

ansible -i hosts -m shell -a 'uname -a' host0.example.org

```

# Playbooks #

```
#!yaml

- name: Проверка модуля shell
  shell: echo "hello world!"

```

## copy ##

** Копирует указный файл на удаленный сервер  **

Пример:

```
#!bash

ansible -i hosts -m copy -a 'src=book.json dest=/tmp/' host0.example.org

```

Playbook:
```
#!yaml

- name: Проверка модуля copy
  copy:
    src=apache.conf
    dest=/etc/apache2/sites-available/
    mode=0640

```

## setup ##
** Полная информация об узлах.  **

Пример:  
Узнать дистрибуцию удаленного узла
```
#!bash

ansible -i hosts -m setup -a 'filter=ansible_distribution' all

```

# Аргументы #

**Последний** параметр который принимает ansible, это то к каким узлам применить изменения.  
Пример: **all**
```
#!bash
ansible -i hosts -m copy -a 'src=book.json dest=/tmp/' all
```
Применить ко всем(all) хостам, инвенторя hosts  
Пример: **one**
```
#!bash
ansible -i hosts -m copy -a 'src=book.json dest=/tmp/' host0.example.org
```
Только хосту host0.example.org, в инвенторе hosts  
Пример: **between**
```
#!bash
ansible -i hosts -m copy -a 'src=book.json dest=/tmp/' host0.example.org:host1.example.org
```
К хосту host0.example.org и к host1.example.org  
Пример: **more**
```
#!bash
ansible -i hosts -m copy -a 'src=book.json dest=/tmp/' host*.example.org
```
Ко всем хостам начианющихся с host и заканчивающихся на example.org  

Пример: **group**  
Также можно группировать узлы в inventory файле  

hosts
```
#!bash
[hosts]
host0.example.org ansible_host=192.168.33.10 ansible_user=root
host1.example.org ansible_host=192.168.33.11 ansible_user=root
host2.example.org ansible_host=192.168.33.12 ansible_user=root
```
Запуск
```
#!bash

ansible -i hosts -m copy -a 'src=book.json dest=/tmp/' hosts
```
Группа hosts  

Пример: **subgroup**  
hosts
```
#!bash
[test:hosts]
hosts

```
Запуск
```
#!bash

ansible -i hosts -m copy -a 'src=book.json dest=/tmp/' test
```
Группировка групп (?)

# Проблемы #

Оочень долго не мог закинуть ssh ключик на узлы командой:
```
#!bash

ansible-playbook -c paramiko -i step-00/hosts step-00/setup.yml --ask-pass --sudo

ansible-playbook -c paramiko -i host ../../base/setup.yml --ask-pass --sudo

```
Помогла очистка файла ~/.ssh/known_hosts

# ansible-playbook [] #
** Выполнение сценариев для ansible **

# Установка #

** Устанавливается вместе с ansible **

# Ключи #

---
* -i
    * Указывает какой "инвертарь" использовать.
* -l
    * Указывает какой узел или группу использовать
*** Последний параметр - yaml файл содержащий ansible сценарий для хостов ***    
---

# Примеры #

# Закинуть свой ssh ключик на несколько машин #

```
#!bash

ansible-playbook -c paramiko -i host setup.yml --ask-pass --sudo

```
где, 
paramiko - ?  
step-00/hosts - файл с указание ip адресов(192.168.33.10, 192.168.33.11, 192.168.33.12) **PS только ip-шники, без доп. параметров**  
step-00/setup.yml - сценарий копирования публичного ssh ключа на виртуальные машины  
--ask-pass --sudo - доп. ключи   

# Проверка работоспособности #
```
#!bash

ansible -m ping all -i host

```

# Установка apache на несколько машин #

```
#!bash

ansible-playbook -i hosts -l tasks.yml

```
где, 
paramiko - ?  
step-00/hosts - файл с указание ip адресов(192.168.33.10, 192.168.33.11, 192.168.33.12)  
step-00/setup.yml - сценарий копирования публичного ssh ключа на виртуальные машины  
--ask-pass --sudo - ?  

# Наброски #

Same issue on 2.2.1. I have roles/project/handlers/main.yml:

- name: Prepare static files
  include: static.yml

- name: Restart uwsgi service
  include: uwsgi.yml
and roles/project/tasks/main.yml:

- name: Clone project repository
  git:
    repo: git@bitbucket.org:company/project.git
    dest: "{{ home_dir }}"
    accept_hostkey: yes
  notify:
    - Prepare static files
    - Restart uwsgi


## Особенности ansible сценариев ##

Везде встречаются сценарии в yaml файлах. Скорее всего это такая особенность ансибла

