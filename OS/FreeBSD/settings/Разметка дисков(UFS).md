# Разметка дисков

## Создание подопытного

1. Создать -> FreeBSD-ZFS -> Далее -> Далее -> Готово
2. Установить 2 ГБ HDD
3. Настройки:  
    a. Носители -> Атрибуты -> Оптический привод -> <путь до iso>  
    b. Сеть:  
        * Включить сетевой адаптер  
        * Тип подключения - Сетевой мост  
        * Имя - еcли Wi-Fi, то wlan0, если провод, то eth  
        * Дополнительно -> Тип адаптера -> Intel PRO 1000 T Server  
4. Запустить

- Shell

Просматриваем, что есть на жёстком диске:
```
gpart show ada0
```

Имена устройств можно посмотреть
```
sysctl kern.disks
```

Создаем таблицу разделов
```
gpart create -s gpt ada0
```

Создаем загрузочный раздел
```
gpart add -t freebsd-boot -l gpboot -s 64k ada0

```
, где:
-t freebsd-boot - тип boot
-l gpboot - лейбл на раздел
-s 64k - размер
ada0 - диск

И что-то еще?
```
gpart bootcode -b /boot/pmbr -p /boot/gptboot -i 1 ada0

```

Создаем свап
```
gpart add -t freebsd-swap -l gpswap -s 128M ada0

```

Создаем подсистемы
```
# основной раздел
gpart add -t freebsd-ufs -l gproot -s 256M ada0 

# для логов
gpart add -t freebsd-ufs -l gpvar -s 256M ada0

# для временных файлов
gpart add -t freebsd-ufs -l gptmp -s 256M ada0

# для общих файлов
gpart add -t freebsd-ufs -l gpusr ada0

```

Монтируем в /mnt
```

mount /dev/gpt/gproot /mnt/

```

TODO: дальше какаята хрень...

https://www.freebsd.org/doc/ru/books/handbook/mount-unmount.html

