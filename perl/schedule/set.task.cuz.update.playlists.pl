#!/usr/bin/env perl

use lib::abs        qw| ../../lib |;
use uni::perl       qw| :dumper |;

use External::Config2;
use FonMix::Schema::MainDB;

my $config = External::Config2->new( project => 'fonmix' );
my $schema = FonMix::Schema::MainDB->connect( @{$config->{'Model::MainDB'}->{'connect_info'}} );
my $dbh = $schema->storage->dbh;

# TODO: проверить нагрузку!
my $schedule_playlists = $dbh->selectall_hashref(
    'SELECT playlist_id, array_agg( schedule_id ) AS schedule_list FROM schedule.playlists WHERE to_publication IS TRUE GROUP BY playlist_id', ['playlist_id']
);

foreach my $playlist_id ( keys %{ $schedule_playlists } ){
    my $task = $dbh->selectrow_hashref(
        'INSERT INTO task.items ( script_id status_id user_id json_params ) VALUES ( ?, ?, ?, ? ) RETURNING *', {},
        2, # repeat.publication
        11, # новая
        $c->user->real_id,
        encode_json({ schedule_list => $schedule_playlists->{$playlist_id}->{schedule_list} }),
    );
}

exit;
