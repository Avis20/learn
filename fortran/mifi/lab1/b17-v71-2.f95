program n_m_1
real, dimension(101) :: arg
real, dimension(101) :: function
real :: f_max=0, f_min=0, avg=0, avg_sqr=0, avg_sqrt=0, pos_nums=0, neg_nums=0, avg_sqrt_diff_avg=0, pi=3.14 
integer :: i_max=0, i_min=0

! вычислим значения Х
do i=0, 100
    arg(i) = i/100.
    print*, arg(i)
enddo

! вычислим значения У
do i=0, 100
! тестовая функция
!   function(i) = (arg(i)*(1-arg(i)))-(1./8)
! по варианту
    function(i) = sin(pi*arg(i)/2.)-(1-arg(i))
enddo

do i=0, 100
! найдем максимальное значение и индекс элемента
    if (function(i)>f_max) then
        f_max = function(i)
        i_max = i+1
    end if
! минимальное
    if (function(i)<f_min) then
        f_min = function(i)
        i_min = i+1
    end if
! среднее значение (пока просуммируем)
    avg = avg+function(i)
! так же просуммируем значения для среднего квадрата
    avg_sqr = avg_sqr+(function(i)**2)
! относительные значения положительных и отрицательных чисел
    if (function(i)>0) then
        pos_nums = pos_nums+1
    else
        neg_nums = neg_nums+1
    end if
enddo

! завершим расчеты
avg = avg/101
avg_sqr = avg_sqr/101
avg_sqrt = sqrt(avg_sqr)
pos_nums = pos_nums/101.
neg_nums = neg_nums/101.

! новый цикл для среднеквадратичного отклонения от среднего значения
do i=0, 100
    avg_sqrt_diff_avg = avg_sqrt_diff_avg+((function(i)-avg)**2)
enddo
avg_sqrt_diff_avg = sqrt(avg_sqrt_diff_avg/101.)

print*, 'максимальное значение и индекс элемента:', f_max, i_max
print*, 'минимальное значение и индекс элемента:', f_min, i_min
print*, 'среднее значение:', avg
print*, 'средний квадрат:', avg_sqr
print*, 'среднеквадратичное значение:', avg_sqrt
print*, 'относительное число положительных:', pos_nums
print*, 'относительное число отрицательных:', neg_nums
print*, 'среднеквадратичное отклонение от среднего значения:', avg_sqrt_diff_avg
end
