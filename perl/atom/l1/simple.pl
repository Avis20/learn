#!/usr/bin/env perl

use uni::perl       qw| :dumper |;

my $var;        # Обьявление переменной
my $a = 42;     # Присвоение переменной значниея
# print "test";   # Вывод в STDOUT
eval {};        # Выполнение кода с крит. ошибкой
do {};          # Просто выполнение блок кода

if ( $a > $b ){
    print "a > b";
}

if ( $a > $b ){
    print "a > b";
} else {
    print "something else";
}

my $c;
if ( $a > $b ){
    print "a > b";
} elsif ( $a > $c ){
    print "a > c";
} else {
    print "something else";
}
unless ( $a > $b ){
    print 'b > a';
}

my $a = 0;
while ( $a < 3 ){
    $a++;
    print "$a\n";
}

my $a = 0;
until ( $a > 3 ){
    $a++;
    print "$a\n";
}

my $a = 0;
do {
    $a++;
    print "$a\n";
} while ($a < 3);

my $a = 0;
do {
    print "$a\n";
    $a++;
} until ( $a > 3 );

for (my $i = 0; $i < 3; $i++){
    print "$i\n";
}

my @list = (1,2,3);
for my $a ( @list ){
    print "$a\n";
}

my @list = (1,2,3);
print "$_\n" for ( @list );

eval "$a/$b";
warn $@ if $@;

eval { $a / $b };
warn $@ if $@;

eval { die "Not root" if $< };
warn $@ if $@;

eval {
    100 / 0;
1 } or do {
    warn "Error: $@";
};

my $a = $b = "test\t\n";
say chop($a) . chop($a) . chop($a); # \n, \t, t
say $a;

say chomp($b) . chomp($b) . chomp($b); # \n, \t, ''
say $b;

#         ↓─────index($_," ") # 4
$_ = "some average string\n";
#        └─┬─┘    ↑───rindex($_," ") # 12
#          substr($_,3,5) = "e ave"

my $big = 'WORD';
my $small = 'word';
say lc $big;        # word "\L"
say lcfirst $big;   # wORD "\l"
say uc $small;      # WORD "\U"
say ucfirst $small; # Word "\u"

say "\u\l$big\E";

say sprintf "%c", 9786;     # ☺ - utf8 символы
say sprintf "%s", "str";    # str - строка
say sprintf "%d", 49;       # 49 - число
say sprintf "%u", -1;       # 18446744073709551615 - TODO: непонятно
say sprintf "%o", 101;      # 145 - 8-ричное число
say sprintf "%x", 57005;    # dead - 16-ричное число
say sprintf "%e", 1/3;      # 3.333333e-01 - вывод дробного числа с экспонентой
say sprintf "%f", 1/5;      # 0.200000
say sprintf "%g", .333333e-01; # 0.0333333

sub mysub {
    my ($a, $b ) = @_;
    my $r = $a + $b;
    return $r;
};

my $var = sub {
    my $a = shift;
    return $a * 2;
};

say mysub( 1, 2 );
say mysub 5, 9;
say $var->(5);

use MP3::Tag;
my $mp3 = MP3::Tag->new( $ARGV[0] );
print $mp3->artist . " - " . $mp3->title . "\n";

use DDP;
my $foo = [{a => 1, b => 2}, { c => 3, d => 4}];
say p $foo;
