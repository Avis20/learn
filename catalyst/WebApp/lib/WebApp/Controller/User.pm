package WebApp::Controller::User;
use Moose;
use namespace::autoclean;
use uni::perl qw/ :dumper /;

BEGIN { extends 'Catalyst::Controller' }

__PACKAGE__->config(namespace => 'user');

sub base :Chained('') :PathPart('user') :CaptureArgs(0) {
    my ( $self, $c ) = @_;
}

1;