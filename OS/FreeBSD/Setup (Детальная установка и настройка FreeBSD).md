Попытка № 9

7 - старая версия - 10.1
8 - не заработал интеренет
9 

### Links ###
* https://download.freebsd.org/ftp/releases/ISO-IMAGES/10.3/ - дистрибутив
* https://wiki.lissyara.su/wiki/FreeBSD_%D0%A0%D1%83%D0%BA%D0%BE%D0%B2%D0%BE%D0%B4%D1%81%D1%82%D0%B2%D0%BE_%D0%9F%D0%BE%D0%BB%D1%8C%D0%B7%D0%BE%D0%B2%D0%B0%D1%82%D0%B5%D0%BB%D1%8F:_%D0%93%D0%BB%D0%B0%D0%B2%D0%B0_15_Jails_(%D0%9A%D0%BB%D0%B5%D1%82%D0%BA%D0%B8) - Руководство на русском
* https://www.youtube.com/watch?v=Ab30k5mvZok&t=120s
* http://www.site-motor.ru/docs/freebsd/jail.shtml
* https://habrahabr.ru/post/342312/.com.1vvedenie - свежачок

1. с п. 1 по п. 2.5 вынес в скрипт - develop/learn/OS/script/freebsd_server.sh

1 Установка (через интерфейс)

    1. Создать->FreeBSD->Далее->Далее->Готово
    1.1. Установить 10 ГБ ПЗУ

    2. Настройки:
    2.1. Общее->Дополнительно->Общий буфер обмена - Двунаправленный, Drag'n'Drop - Двунаправленный
    --2.2. Носители->Атрибуты->Оптический привод-><путь до iso>--
    2.3. Сеть
        - Включить сетевой адаптер
        - Тип подключения - Сетевой мост
        - Имя - еcли Wi-Fi, то wlan0, если провод, то eth
        - Дополнительно -> Тип адаптера -> Intel PRO 1000 T Server

    2.4. Общие папки
        - Добавить общую папку(папка с плюсиком с права)
        - Путь /home/avis/VB/freebsd
        - Авто-подключение +
        - Создать постоянную папку +

    2.5. Запустить

    3 установка
    3.1. Ввести хост машины - free_host
    3.2. Distribution selected. Выбрать - games, src
    3.3. Разметка диска:
        - Partition
        - Delete
        - Auto
        - Partition
        - Finish
        - Commit

    3.4. Ждемс
    3.5. дальше по деволту
    3.6. пароль руту
    3.7. Интернет
        - IPv4 - yes
        - DHCP - no
        - ip - 172.16.11.44
        - default router - 172.16.10.1

    3.8. Добавить другого пользователя:
        - avis
        - wheel
        по дефолту
    3.6. Reboot,

    4. Лишнее
    4.1. Настройки->Носители->Удалить iso образ, а то будет спрашивать при запуске

    5. Запуск машины. Настройка
    5.1. Инета так и нет, пробуем починить
        Пробуем 1 из вариантов media: autoselect, 10baseT/UTP и 100baseTX.
        - ifconfig em0 media 100baseTX
        - dhclient em0
        - ping 8.8.8.8
        - ifconfig em0
        Ура! заработало!

    5.2. SSH
        Да собственно ничего и не надо, т.к. во время установки мы указали ssh :)
        Но можно проверить:
        cat /etc/rc.conf
        Если есть sshd_enable="YES" то все ok

        Изменяем чтоб заходить под рутом
        «PermitRootLogin yes» в /etc/ssh/sshd_config

        Сразу закидываем публичный ключ, чтоб пароль по 100 раз не водить
        cat ~/.ssh/id_rsa.pub | ssh root@172.16.11.39 "mkdir ~/.ssh/; cat >> ~/.ssh/authorized_keys"

        И sshfs чтоб просматировать файлики
        mkdir rootcom/my/freebsd
        sshfs root@172.16.11.39:/ /home/avis/rootcom/my/freebsd/ -o uid=1000,gid=1000 -p 22
    
    6 ansible
        (не нашел как это сделать удаленно!)
        7.1 Устанавливаем менеджер пакетов - просто вызвать 
            pkg 
        7.2 И питон для ансибла
            pkg-static search python2-2_3
        7.3 Проверить что все хорошо
            ansible -m ping all -i host
    
    Запуск
        ansible-playbook pkg.yaml -i host
        

    6 Jails Настройка - https://habrahabr.ru/post/342312/
        Считаем, что:
        - сетевой интерфейс: em0
        - наш основной ip: 172.16.11.43
        - клетки называются jail1 и jail2
        - ip клеток: 172.16.11.33 и 172.16.11.34
        
        Дальше по инструкции
        http://www.site-motor.ru/docs/freebsd/jail.shtml

Bags

    Тупит при старте, при выборе DHCP

Результаты

    Попытка № 1.1.3

    +
    Система установлена

    -
    Нет инета
    ssh соединения
    гостевой системы
    нет джейлов
    нет веб сервера

    Попытка № 1.1.6

    +
    Починил инет
    ssh
    sshfs

    -
    рут доступен всем
    нет джейлов
    нет веб сервера

# Джейлы #

Создание и управление клетками


```
#!bash

mkdir -p /usr/jail/jail1
cd /usr/src
make buildworld

make installworld DESTDIR=$D (3)
make distribution DESTDIR=$D (4)
mount -t devfs devfs $D/dev (5)

```


# Проблемы #

avis:
Почему-то во при установки 10.1 все было ok, но 10.3 после получения ip-шника не работал инет. 
Инструкция по починке - https://adw0rd.com/2009/06/17/freebsd-no-route-to-host/
Починил так:
Проверил что действительно так
$ netstat -r | default
через домашнюю машину узнал ip-шник роутера
$ ip route show | grep default
Потом добавил его в таблицу маршрутизации на виртуалке
$ route add default 10.20.30.1
и вписал его в rc-шник
$ vim /etc/rc.conf 
defaultrouter="10.20.30.1"


* ifconfig em0 media 100baseTX
* dhclient em0
* ping 8.8.8.8
* ifconfig em0






03.12.2017
---

Вчера я при установке 10.3 установил статический ip-шник - 172.16.11.44
И сорее всего он был занят на фряхе не было инета. При получении по dhcp получалось 2 ip-шника на 1 интерфейс
Поправил добавив в /etc/rc.conf

ifconfig_em0="DHCP"
defaultrouter="172.16.10.1"  - по поводу роутера не уверен, возможно он и так будет работать

---
Нужно открыть доступ руту по ssh

Изменяем чтоб заходить под рутом
vi /etc/ssh/sshd_config

PermitRootLogin yes

Сразу закидываем публичный ключ, чтоб пароль по 100 раз не водить
cat ~/.ssh/id_rsa.pub | ssh root@172.16.11.46 "mkdir ~/.ssh/; cat >> ~/.ssh/authorized_keys"

И sshfs чтоб просматировать файлики
mkdir rootcom/my/freebsd
sshfs root@172.16.11.39:/ /home/avis/rootcom/my/freebsd/ -o uid=1000,gid=1000 -p 22

---
Джейлы

make distribution DESTDIR=/usr/jails/jail1


---
Настройка запуска jail-ов

sysrc jail_enable="YES"
sysrc rctl_enable="YES"
sysrc rctl_rules="/etc/rctl.conf"
sysrc zfs_enable="YES"
sysrc ifconfig_em0_alias="192.168.1.105/24"

Считаем, что:
- сетевой интерфейс: em0
- наш основной ip: 172.16.11.44
- клетки называются jail1 и jail2
- ip клеток: 192.168.200.1 и 192.168.200.2 

/etc/rc.conf (добавить или поменять)
ifconfig_em0_alias0="inet 192.168.200.1 netmask 255.255.255.0"
ifconfig_em0_alias1="inet 192.168.200.2 netmask 255.255.255.0"

jail_enable="YES"
jail_list="jail1 jail2"

syslogd_flags="-s -b 192.168.100.1"



/etc/jail.conf - файл с настройками наших клеток.
exec.start = "bin/sh /etc/rc";
exec.stop = "/bin/sh /etc/rc.shutdown";
exec.clean;
mount.devfs;
allow.mount;
path = "/jails/$name";
interface = "rl0";

jail1 {
 host.hostname = "jail1.localnet";
 ip4.addr = "192.168.200.1";
 ip6.addr = "";
 mount.fstab = "/etc/fstab.jail1";
}
jail2 {
 host.hostname = "jail2.localnet";
 ip4.addr = "192.168.200.2";
 ip6.addr = "";
 mount.fstab = "/etc/fstab.jail2";
}

---
Установка пакетов



printf "\n${ORANGE}\t/etc/rc.conf${NC}\n\n"

# # Автозапуск sshd
# ssh root@${IP} -p 22 sysrc sshd_enable="YES"

# # Установите в NO для отключения запуска любой  клетки
# ssh root@${IP} -p 22 sysrc jail_enable="YES"

# # _aliasX - прописываем статические ip для клеток
# ssh root@${IP} -p 22 'sysrc ifconfig_em0_alias0="inet 192.168.200.1 netmask 255.255.255.0"'
# ssh root@${IP} -p 22 'sysrc ifconfig_em0_alias1="inet 192.168.200.2 netmask 255.255.255.0"'

# # список разделенных пробелами имен клеток
# ssh root@${IP} -p 22 'sysrc jail_list="jail1 jail2"'

# ssh root@${IP} -p 22 sysrc syslogd_flags="-s -b 192.168.100.1"

printf "\n${ORANGE}\t/etc/jail.conf${NC}\n\n"

# /etc/jail.conf - файл с настройками наших клеток.
ssh root@${IP} -p 22 'cat <<EOF > /etc/jail.conf
exec.start = "bin/sh /etc/rc";
exec.stop = "/bin/sh /etc/rc.shutdown";
exec.clean;
mount.devfs;
allow.mount;
path = "/usr/jails/$name";
interface = "em0";

jail1 {
    host.hostname = "jail1.localnet";
    ip4.addr = "192.168.200.1";
    ip6.addr = "";
    mount.fstab = "/etc/fstab.jail1";
}
jail2 {
    host.hostname = "jail2.localnet";
    ip4.addr = "192.168.200.2";
    ip6.addr = "";
    mount.fstab = "/etc/fstab.jail2";
}
EOF'




```
#!bash
this
```


* python27
* bash
* sudo
* ezjail
* postfix
* zabbix34-agent
* rsync wget


