
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

//#1: Define the HTML Webpack Plugin:
const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
    filename: 'index.html',
    inject: 'body',
    title: 'HELLO',

// Here is part of the magic, we get the index.njk but we tell
// webpack to pass it through the nunjucks-html-loader
    template: 'nunjucks-html-loader!./source/templates/index.njk',
  });

// Optional, but highly recommended. Create a returnEntries:
// Webpack doesn't support glob paths. For the nunjucks-html-loader
// we need each path to be specified for it to work (YES, even subdirectories!)

module.exports = {
// You should not have this the same. This is from my site. Go down to see the important part:
    entry: './source/index.js',
    output: {
      filename: 'bundle.js',
      path: __dirname + '/dist',
      publicPath: 'dist/' //Important!!! : https://github.com/webpack/webpack/issues/1426
    },
   // #2  We load the HTMLWebpackPluginConfig
    plugins: [
      HtmlWebpackPluginConfig
    ],

    resolve: {
      extensions: ['.Webpack.js', '.web.js', '.ts', '.js', '.tsx']
    },

// HERE is the important part
    module: {
      loaders: [
        {
          // HTML LOADER
          // Super important: We need to test for the html 
          // as well as the nunjucks files
          test: /\.html$|njk|nunjucks/,
          use: ['html-loader',{
            loader: 'nunjucks-html-loader',
            options : {
               // FileSystemLoader: '/home/avis/develop/learn/js/nunjucks',
               // Other super important. This will be the base
               // directory in which webpack is going to find 
               // the layout and any other file index.njk is calling.
               // searchPaths: ['/home/avis/develop/learn/js/nunjucks/source/templates/**/']
               searchPaths: ['source/templates']
               // Use the one below if you want to use a single path.
               // searchPaths: ['./source/templates'],
            }
          }]
        }
        ]
    }
}