#!/usr/bin/env perl

use uni::perl       qw| :dumper |;
use AnyEvent;
use Coro;



=head coro 
async {
    say 2;
    cede;
    say 4;
}

say 1;
cede;
say 3;
cede;

=head
# warn rand(0.1);
# =head
my $cv = AE::cv {
    say "cv done";
}

sub async {
    my $cb = pop;

    my $w;
    $w = AE:: timer 1, 0, sub {
        undef $w;
        $cb->();
    }
    return;

}



=head

my $cv = AE::cv {
    say "cv done";
}

$cv->recv;

=head
my $cv = AE::cv();

my $p;
$p = AE::timer 0, 0.1, sub {
    state $count = 0;
    warn $count;
    if (++$count > 5){
        undef $p;
        $cv->send;
        return;
    }
};

$cv->recv;

=head
sub dec {
    my $decor = shift;
    return sub {
        return $decor . "@_" . $decor;
    }
}

my $dq = dec "'";
say $dq->('test');

exit;