# VirtualBox - как управлять через shell #

# Links #
* https://blog.sleeplessbeastie.eu/2013/07/23/virtualbox-how-to-control-virtual-machine-using-command-line/
* https://www.virtualbox.org/manual/ch08.html
* https://www.perkin.org.uk/posts/create-virtualbox-vm-from-the-command-line.html
* https://mezzantrop.wordpress.com/2016/10/18/quick-reference-for-adding-and-attaching-drives-in-virtualbox-cli/
* http://eax.me/vboxmanage/
* https://habrahabr.ru/post/77834/
* https://coderwall.com/p/8m--dq/purge-deleted-hard-disks-from-virtual-box

# Создание и настройка новой vm #

## VBoxManage createvm - Создание ##

```
#!bash
VBoxManage createvm --name "test" --register
```

## VBoxManage showvminfo - Посмотреть подробную информацию о vm ##

```
#!bash
avis@PC:~$ VBoxManage showvminfo test
Name:            test
Groups:          /
Guest OS:        Other/Unknown
UUID:            57a85c04-1cf7-478e-93ab-e15dec3c922e
Config file:     /home/avis/VirtualBox VMs/test/test.vbox
...
```


## VBoxManage modifyvm - Изменить настройки ##


```
#!bash
VBoxManage modifyvm test --memory 512
```

---

# Списки #

## VBoxManage list vms - Список установленных машин ##

```
#!bash

avis@PC:~$ VBoxManage list vms
"Windows XP" {cd2a9e96-f262-427a-92b1-45176796a870}
"host0" {f8c8933f-337f-4daf-bd53-cb7a5144549e}
"host1" {ae0202ce-3b89-449e-a76d-4decd4f9f088}
"host2" {87cdab39-d1a4-4c1d-9e70-755a217f8156}
"64_default_1498752896769_36007" {83348e1b-4900-447a-9df9-4bd1c42be78a}
"LBox" {627baa8a-17d9-4b27-b420-f866f1d961a0}
"FreeBSD" {dd885030-392a-4b78-8f14-d8be098eab17}

```

## VBoxManage list runningvms - Список запущеных машин ##

```
#!bash
avis@PC:~$ VBoxManage list runningvms
"FreeBSD" {dd885030-392a-4b78-8f14-d8be098eab17}
```


## VBoxManage list hdds runningvms - Список hdd ##

```
#!bash
avis@PC:~$ VBoxManage list hdds
UUID:           afb5bfa8-4909-4aa4-b823-32c1d5fc7394
Parent UUID:    base
State:          created
Type:           normal (base)

```

# Запуск #

## VBoxManage startvm - GUI ##

```
#!bash
avis@PC:~$ VBoxManage startvm FreeBSD
Waiting for VM "FreeBSD" to power on...
VM "FreeBSD" has been successfully started.
```

## VBoxSDL --startvm - GUI, с привязкой к bash + без контролов ##

```
#!bash
avis@PC:~$ VBoxSDL --startvm FreeBSD
VBoxSDL --startvm FreeBSD-ZFS
Oracle VM VirtualBox SDL GUI version 5.0.40_Ubuntu

```

## VBoxHeadless --startvm - без GUI ##

```
#!bash
avis@PC:~$ VBoxHeadless --startvm FreeBSD
```

## VBoxManage controlvm reset - Перезагрузить машину ##

```
#!bash
avis@PC:~$ VBoxManage controlvm FreeBSD reset
```

## VBoxManage controlvm poweroff - Выключить машину ##
```
#!bash
avis@PC:~$ VBoxManage controlvm FreeBSD poweroff
```


---
# Жеские диски - HDD #

## hdd на 10 гб. ##
```
#!bash
$ vboxmanage createhd \
    --filename "/home/avis/VirtualBox VMs/test/test.vdi" \
    --size 10000;
```

## Цепляем hdd к vm ##
```
#!bash

$ vboxmanage storagectl ubuntu --name "IDE Controller" --add ide;

$ vboxmanage storageattach ubuntu --storagectl "IDE Controller" \
    --port 0 --device 0 --type hdd \
    --medium "/home/avis/VirtualBox VMs/test/test.vdi";
```

## Удалить hdd ##
```
#!bash

$ vboxmanage closemedium disk <uuid> --delete

```

# Busg #

При установке ubuntu-server, нужно выделять RAM 512M+. А то будет вылетать с ошибкой во время установки - не удалось загрузить apt-cdrom-setup