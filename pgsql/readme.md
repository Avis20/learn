
# Links #
* http://postgresql.ru.net/manual/tutorial.html
* https://habrahabr.ru/post/168601/
* https://www.postgresql.org/ftp/source/ - список исходников

# База #

## createdb - Создать БД ##

```
#!bash

createdb mydb

```
Чтобы создать одноимённую с именем пользователя базу данных, просто - createdb

## psql -l - Список баз ##

```
#!bash

postgres@ubuntu2:~$ psql -l
                                  List of databases
   Name    |  Owner   | Encoding |   Collate   |    Ctype    |   Access privileges   
-----------+----------+----------+-------------+-------------+-----------------------
 mydb      | postgres | UTF8     | ru_RU.UTF-8 | ru_RU.UTF-8 | 
 postgres  | postgres | UTF8     | ru_RU.UTF-8 | ru_RU.UTF-8 | 
 template0 | postgres | UTF8     | ru_RU.UTF-8 | ru_RU.UTF-8 | =c/postgres          +
           |          |          |             |             | postgres=CTc/postgres
 template1 | postgres | UTF8     | ru_RU.UTF-8 | ru_RU.UTF-8 | =c/postgres          +
           |          |          |             |             | postgres=CTc/postgres
(4 rows)

```

## dropdb - Удалить БД ##

```
#!bash

dropdb mydb

```

## psql - Получить интерактивный доступ к БД ##

```
#!bash

postgres@ubuntu2:~$ psql mydb 
psql (9.5.9)
Type "help" for help.

mydb=#

```

# psql #

* http://postgresql.ru.net/manual/app-psql.html

## \h - Справка ##

```
#!bash

mydb=# \h

```

## \q - Выйти из БД ##

```
#!bash

mydb=# \q

```

## \i - Читать команды из файла ##

```
#!bash

mydb=> \i basics.sql

```

или

```
#!bash

psql -f basics.sql

```

## \dt - Список таблиц ##

```
#!bash

postgres=# \dt;
        List of relations
 Schema | Name | Type  |  Owner   
--------+------+-------+----------
 public | pet  | table | postgres
(1 row)

```

## \d - Структура таблицы ##

```
#!bash

postgres=# \d pet ;
             Table "public.pet"
 Column |         Type          | Modifiers 
--------+-----------------------+-----------
 name   | character varying(20) | 
 owner  | character varying(20) | 
 type   | character varying(20) | 
 sex    | character(1)          | 
 birth  | date                  | 
 death  | date                  | 

\d: extra argument ";" ignored

```


# Таблицы #

## create table - Создать таблицу ##

```
#!bash

create table pet (
    name varchar(20),
    owner varchar(20),
    type varchar(20),
    sex char(1),
    birth date,
    death date
);

```

## drop table - удалить таблицу ##

```
#!bash

postgres-# drop table pet;
DROP TABLE

```

# Вставка данных #

## insert - вставка одиночных данных ##

```
#!bash

insert into pet values ('puffball', 'Diane', 'hamster', 'f', '2017-03-30', null);

```

## load data local - загрузить из файла ##

```
#!bash

load data local infile '/home/avis/develop/learn/mysql/pet.txt' into table pet;

```


# FAQ #

    Q: как установить?
    A: Если из пакетов:

```
#!bash

sudo apt-get install postgresql-9.5

```

    A: Если собирать из исходников:

```
#!bash

wget http://ftp.postgresql.org/pub/source/v9.2.2/postgresql-9.2.2.tar.gz
tar xzf postgresql-9.2.2.tar.gz

# нужен C компилятор чтоб собрать
sudo apt install gcc

cd postgresql-9.2.2
./configure --without-readline
sudo make install clean

aaa, дальше лень писать, установи из пакетов.
```

    Q: а дельше че?
    A: нужно создать пользователя для доступа к кластеру(чтобы это ни значило)

```
#!bash



```


    Q - Что такое большие страницы: huge_pages
    A - ХЗ


# Bags #

Пока нет

# Breakpoint #

* http://postgresql.ru.net/manual/tutorial-populate.html
