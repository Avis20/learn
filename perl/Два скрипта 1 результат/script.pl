#!/usr/local/bin/perl
use strict;
use warnings;
require 'CheeseSpread.pl';

print name();
print monkish();

sub name{
        return "my name is $CheeseSpread::name\n";
        }
sub monkish {
        return "my avatar is $CheeseSpread::monkish\n";
        }
print "-" x 50, "\n";   # a horizontal rule

my $firstName = $CheeseSpread::name;
my $monasteryName = $CheeseSpread::monkish;
print "my name is $firstName\n";
print "my avatar is $monasteryName\n";

1;
