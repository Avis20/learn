#!/usr/bin/perl -I/home/avis/perl5/lib/perl5/

use uni::perl qw/:dumper/;
use Sphinx::Search;

my $config = {
    Sphinx => {
        host => 'localhost',
        port => 9312,
    },
};

my $sphinx = Sphinx::Search->new();
$sphinx->SetServer( $config->{Sphinx}->{host}, $config->{Sphinx}->{port} );
my $query = $sphinx->SetSortMode( SPH_SORT_RELEVANCE );

my $sphinx_results = $query->Query( 'test', 'test1' );

warn dumper $sphinx_results;

exit;