#!/usr/bin/perl

use Encode  qw| encode decode |;
use Log::Dispatch;

# simple
my $log = Log::Dispatch->new(
    outputs => [
        [
            'File',
            min_level => 'debug', filename => 'logfile.txt', 
            mode      => 'append' ,
            TZ        => 'AEDT',
            size      => 1,
            max       => 6,
            DatePattern => 'yyyy-dd-HH'
        ],
        [ 'Screen', min_level => 'warning' ],
    ]
);

write_to_log('info', "Эта строка отправится в файл\n");
write_to_log('warn', "Привет!");

sub write_to_log {
    my ( $level_name, $message ) = @_;

    my ( $sec, $min, $hour, $day, $month, $year ) = localtime();
    my $ts = sprintf "%02d-%02d-%04d %02d:%02d:%02d", $day, ++$month, ( $year += 1900 ), $hour, $min, $sec;

    $log->log( level => $level_name, message => "$ts [$level_name] $message\n" );
}
