#!/usr/bin/perl

use uni::perl       qw| :dumper |;
use Template;

my $tt = Template->new();

$tt->process(
    'template/test.tt', {
        data => {
            name => 'Avis',
            age => '21',
        }
    }) or die $tt->error();
