program loop_step
! Using real step size in loops
implicit none

real :: x, x_min, x_max, step
integer :: i, n

x_min = 0
x_max = 1
step = 0.1
10 format(i2 a6 f3.1)

write (*,*) 'Using DO loop with control variable:'
x = x_min
n = nint((x_max - x)/step)
print *, n
do i=1, n+1
  write(*,10) i, ". x = ", x
  x = i*step
end do
write(*,*)

end  