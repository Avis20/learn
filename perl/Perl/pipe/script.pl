#!/usr/bin/perl

use uni::perl qw /:dumper/;

use IO::Handle;         # подключаем стандартный модуль

pipe(READER, WRITER);   # создаем программный канал
WRITER->autoflush(1);   # включаем авто-очистку буфера
my $pid;
if ($pid = fork()) {    # процесс-предок получает PID потомка
    close READER;        # предок не будет читать из канала
    print WRITER "Послано предком (PID $$):\n";
        for (my $n = 1; $n <= 5; $n++) { # запись в передатчик 
        print WRITER "$n ";
    }
    close WRITER;        # закрываем канал и
    waitpid $pid, 0;     # ждем завершения потомка
} 
die "fork не отработал: $!" unless defined $pid;
if (!$pid) {            # процесс-потомок получает 0
    close WRITER;        # предок не будет писать в канал
    print "Потомок (PID $$) прочитал:\n";
    while (my $line = <READER>) { # чтение из приемника
    print "$line";
    }
    close READER;        # канал закрывается
    exit;                # потомок завершает работу
}
