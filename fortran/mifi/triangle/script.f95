program trangle3

real a, b, c;
real :: perimeter, half_perimeter, square, max_side, min_side, middle_side;

print *, 'Введите три числа';
read *, a, b, c;

if ( a + b > c .and. a + c > b .and. a + c > a ) then
    print *, 'Трехугольник существует';

    perimeter = a + b + c;
    print *, 'Периметр трехугольника: ', perimeter;

    ! площадь расчитывается по формуле Герона
    half_perimeter = perimeter / 2;
    square = sqrt( half_perimeter * ( (half_perimeter - a) * (half_perimeter - b) * (half_perimeter - c) ) );
    print *, 'Площадь трехугольника: ', square;

    if ( a == b .and. a == c .and. b == c ) print *, 'Равносторонний';
    if ( ( a == b .or. a == c ) .or. ( b == a .or. b == c ) .or. ( c == a .or. c == b ) )  print *, 'Равнобедренный';

    max_side = a;
    if ( b > a .and. b > c ) max_side = b;
    if ( c > a .and. c > b ) max_side = c;

    min_side = a;
    if ( b < a .and. b < c ) min_side = b;
    if ( c < a .and. c < b ) min_side = c;

    print *, 'макс. - ', max_side, ' мин. - ', min_side;

    middle_side = perimeter - max_side - min_side;
    d = min_side**2 + middle_side**2;
    e = max_side**2;

    if ( abs( d - e) < 0.000001 ) then
        print *, 'Прямоугольный';
    else if ( d > e ) then
        print *, 'Остроугольный';
    else if ( d < e ) then
        print *, 'Тупоугольный';
    endif
else
    print *, 'Нет такого трехугольника';
end if

end program trangle3