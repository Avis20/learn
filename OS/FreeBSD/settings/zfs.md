
# ZFS


```
# Ищем диск
root@freebsd11:~ # sysctl kern.disks
kern.disks: ada2 ada1 ada0 cd0

# Создаем таблицу разделов
gpart create 

```

## Links

[Что такое ZFS](https://docs.oracle.com/cd/E19253-01/820-0836/zfsover-2/index.html)
[ZFS на Linux Debian 8. Часть 1](https://www.youtube.com/watch?v=ZhlkwTNkh58&list=PLFHbPE9PGy7UNZb0IkpJc8KB_drOIL64M&t=1221s&index=10)
[Установка FreeBSD+ZFS](https://www.youtube.com/watch?v=j06efTacH3E&t=36s)
[confluence](https://confluence.prototypes.ru/x/C4-/)
[Установка FreeBSD на ZFS включая корневой раздел](https://deathstar.name/ustanovka-freebsd-na-zfs-vklyuchaya-kornevoj-razdel/)
(Установка FreeBSD на ZFS/root или Пособие себе на память - 1)[http://www.lissyara.su/articles/freebsd/file_system/root_zfs_gpt/]
(Шпаргалка-по-zfs)[http://dreamcatcher.ru/2010/01/10/%D0%A8%D0%BF%D0%B0%D1%80%D0%B3%D0%B0%D0%BB%D0%BA%D0%B0-%D0%BF%D0%BE-zfs/]
