#!/usr/bin/env perl

use uni::perl       qw| :dumper |;
use Encode          qw| encode decode |;
use JSON::XS        qw| decode_json encode_json |;

open my $fh, 'test.json' or die "Can't open test.json: $!";
my @test = <$fh>;
my $hash = decode_json( $test[0] );

warn "\n\n";
warn dumper $hash;
warn "\n\n";

close $fh;

exit;