#!/usr/bin/env perl

use uni::perl       qw| :dumper |;
use Encode          qw| encode decode |;
use JSON::XS        qw| decode_json encode_json |;

my $test = [1,2,3];
for my $index ( 0..$#$test ){
    print 'index = ' . $index . '; el = ' . $test->[$index] . "\n";
};

exit;