#!/usr/bin/env perl

use uni::perl       qw| :dumper |;

my @array1 = (
    [1, 2, 3 ],
    [4, 5, 6 ],
    [7, 8, 9 ]
);

my $array2 = [
    [9, 8, 7],
    [6, 5, 4],
    [3, 2, 1]
];

warn $array1[1][1]; # 5
warn $array2->[1][1]; # 5

exit;