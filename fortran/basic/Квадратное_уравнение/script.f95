
integer a, b, c;
real D, x1, x2

read *, a;

if ( a == 0 ) then
    print *, 'a должно быть польжительным!';
    stop
end if

read *, b, c;
D = b**2 - 4 * a * c;

print *, D;

if ( D > 0 ) then

    x1 = ( -b + sqrt(D) ) / 2.0 * a;
    x2 = ( -b - sqrt(D) ) / 2.0 * a;
    write (*, "(2(A, F8.1))") 'x1 = ', x1, char(10) // char(13) // 'x2 = ', x2;

elseif ( D == 0 ) then

    x1 = ( -b + sqrt(D) ) / 2.0 * a;
    write (*, "(2(A, F8.1))") 'x1 = ', x1;

else

    print *, 'Решений нет?';

end if

end
