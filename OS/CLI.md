
# gpart [действие] [опции] [диск]

Программа для разметки дисков
gpart - “guess PC-type hard disk partitions” - “угадай тип раздела жесткого диска”

## create - создает таблицу разделов
~~~
    -t - тип:
         gpt
~~~

## add - добавляет новый раздел
~~~
    -t - тип 
    -l - установить лейбл
    -s - размер в килобайтах
    -i - порядковый номер
    -F - 
~~~

## bootcode - загрузочный код ?
~~~
    -b - 
    -p - 
~~~

## delete - удаляет раздел
~~~
    -i - индекс удаляемого раздела
~~~

destroy - очищает файловую систему
~~~
    -F - 
~~~

# newfs [опции]
Форматирование разделов

~~~
    -U - 
~~~

# mount [опции] [от куда] [куда]

Монтирование дисков

## Опции
~~~
    -
~~~

# df [опции]

Показывает сведения о файловой системе

## Опции
~~~
    -h, --human-readable - показывает размеры в человеко читаемом формате
~~~

# mkdir [опции] [название директории]...

Создаение директории

## Опции
~~~
    -p - создание "родителей"
~~~
--------------------------------------------------------------------------------------

# zfs [действие] [диск] [опции]

list - список файловых систем
~~~

root@freebsd11:~ # zfs list
NAME     USED  AVAIL  REFER  MOUNTPOINT
test01    74K   880M    23K  /test01
test02   293K   880M    23K  /test02

~~~

create - создание новой файловой системы
~~~

# Создание вложенных фс
root@freebsd11:~ # zfs create -p test01/data/www
root@freebsd11:~ # ls -l /test01/data/www/
total 0

# создание без точек монтирования
root@freebsd11:~ # zfs create -o mountpoint=none test01/home 
root@freebsd11:~ # zfs list
NAME              USED  AVAIL  REFER  MOUNTPOINT
test01            168K   880M    23K  /test01
test01/data        46K   880M    23K  /test01/data
test01/data/www    23K   880M    23K  /test01/data/www
test01/home        23K   880M    23K  none
test02            293K   880M    23K  /test02

~~~

destroy - удаление фс
~~~
root@freebsd11:~ # zfs destroy test01/home
root@freebsd11:~ # zfs list
NAME              USED  AVAIL  REFER  MOUNTPOINT
test01            132K   880M    23K  /test01
test01/data        46K   880M    23K  /test01/data
test01/data/www    23K   880M    23K  /test01/data/www
test02            293K   880M    23K  /test02

# Удаление рекурсивно
root@freebsd11:~ # zfs destroy -r test01/data
root@freebsd11:~ # zfs list
NAME     USED  AVAIL  REFER  MOUNTPOINT
test01  87.5K   880M    23K  /test01
test02   293K   880M    23K  /test02
~~~


Опции:
~~~
    -p - создать "родителей"
    -r - удалить рекурсивно
~~~
--------------------------------------------------------------------------------------

# zpool [действие] [опции]

## create - создает пул из 1 или нескольких жеских дисков
~~~

# создаем пул с разделенным методом т.е. часть на 1 диск часть на другой
root@freebsd11:~ # zpool create test01 /dev/ada1 /dev/ada2 
root@freebsd11:~ # zpool list
NAME     SIZE  ALLOC   FREE  EXPANDSZ   FRAG    CAP  DEDUP  HEALTH  ALTROOT
test01  1.97G    93K  1.97G         -     0%     0%  1.00x  ONLINE  -

# создание зеркального пула. Все данные зеркалируются на оба диска
root@freebsd11:/ # zpool create test02 mirror /dev/ada1 /dev/ada2 
root@freebsd11:/ # zpool status
  pool: test02
 state: ONLINE
  scan: none requested
config:

    NAME        STATE     READ WRITE CKSUM
    test02      ONLINE       0     0     0
      mirror-0  ONLINE       0     0     0
        ada1    ONLINE       0     0     0
        ada2    ONLINE       0     0     0

errors: No known data errors

~~~

## add - добавляет новый диск в пул
~~~

root@freebsd11:~ # zpool list
NAME     SIZE  ALLOC   FREE  EXPANDSZ   FRAG    CAP  DEDUP  HEALTH  ALTROOT
test01  1008M   522K  1007M         -     0%     0%  1.00x  ONLINE  -
root@freebsd11:~ # zpool add test01 /dev/ada2
root@freebsd11:~ # zpool list
NAME     SIZE  ALLOC   FREE  EXPANDSZ   FRAG    CAP  DEDUP  HEALTH  ALTROOT
test01  1.97G   104K  1.97G         -     0%     0%  1.00x  ONLINE  -

~~~

## detach - отсоединяет диск от пула. Работает в mirror режиме
~~~

root@freebsd11:/ # zpool detach test02 /dev/ada1 
root@freebsd11:/ # zpool status
  pool: test02
 state: ONLINE
  scan: none requested
config:

    NAME        STATE     READ WRITE CKSUM
    test02      ONLINE       0     0     0
      ada2      ONLINE       0     0     0

errors: No known data errors

~~~

## attach - присоединяет диск от пула. Работает в mirror режиме
~~~

root@freebsd11:/ # zpool attach test02 /dev/ada2 /dev/ada1 
root@freebsd11:/ # zpool status
  pool: test02
 state: ONLINE
  scan: resilvered 78.5K in 0h0m with 0 errors on Wed Jan  3 02:21:44 2018
config:

    NAME        STATE     READ WRITE CKSUM
    test02      ONLINE       0     0     0
      mirror-0  ONLINE       0     0     0
        ada2    ONLINE       0     0     0
        ada1    ONLINE       0     0     0

errors: No known data errors

~~~

## replace - заменяет диски
~~~

root@freebsd11:/test01 # zpool replace test01 /dev/ada1 /dev/ada3 
root@freebsd11:/test01 # zpool status
  pool: test01
 state: ONLINE
  scan: resilvered 58.5K in 0h0m with 0 errors on Wed Jan  3 02:11:23 2018
config:

    NAME        STATE     READ WRITE CKSUM
    test01      ONLINE       0     0     0
      ada3      ONLINE       0     0     0
      ada2      ONLINE       0     0     0

errors: No known data errors

~~~

## status - выводит подробный статус пула
~~~
root@freebsd11:~ # zpool status
  pool: test01
 state: ONLINE
  scan: none requested
config:

    NAME        STATE     READ WRITE CKSUM
    test01      ONLINE       0     0     0
      ada1      ONLINE       0     0     0
~~~

## list - выводит список пулов
~~~
root@freebsd11:~ # zpool list
NAME     SIZE  ALLOC   FREE  EXPANDSZ   FRAG    CAP  DEDUP  HEALTH  ALTROOT
test01  1008M    86K  1008M         -     0%     0%  1.00x  ONLINE  -
test02  1008M   500K  1008M         -     0%     0%  1.00x  ONLINE  -
~~~


## destroy - удалить пул. ! При удалении пула, удаляется и фс !
~~~

root@freebsd11:~ # zpool destroy test01
root@freebsd11:~ # zpool list
NAME     SIZE  ALLOC   FREE  EXPANDSZ   FRAG    CAP  DEDUP  HEALTH  ALTROOT
test02  1008M   500K  1008M         -     0%     0%  1.00x  ONLINE  -

~~~


## dd if=[откуда] of=[куда] [опции]

Копирование данных побайтово

~~~

# Скопировать из /dev/urandom в /dev/null 5 раз по 100Мб
dd if=/dev/urandom of=/dev/null bs=100M count=5

~~~
--------------------------------------------------------------------------------------

# mkfile [опции] [размер][ед. измерения] [путь до файла]

Создание файла указанного размера

опции
~~~

~~~

ед. измерения:

~~~
  e
  p
  t
  g
  m - мегабайты
  k
  b
~~~

Пример:
~~~

# Создать файл /test/foo, размером 100 мб.
mkfile 100m /test/foo

~~~


Установка:
- FreeBSD

~~~

~~~