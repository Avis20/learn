#!/usr/local/bin/bash

mode=$1
players=1
playlist=856
count_req=10
time=60

url="http://dev.qa.fonmix.ru/api/yellow/cabinet/filial/stat";

if [[ $mode == "qa" ]]; then

    echo "Запуск теста на qa. ";
    for (( i = 1; i <= ${players}; i++ )); do
        echo " ${i} users, ${count_req} requests ";
        siege -c${i} -r ${count_req} -v -lqa3.txt --mark="${i} users" \
        # siege -c${i} -t60S -v -lqa3.txt --mark="${i} users" \
        -H 'X-Player-Secret-Key:579f516a-c7a8-c5bc-05c9-f3ee05cb9a15' \
        -H 'X-Player-Device-Key:b283adad02be6f1d62146c2ec6293d97' \
        -H 'X-Player-Version:win_7_1.1.0.26' \
        ${url}
    done

elif [[ $mode == "release" ]]; then

    echo "Запуск теста на ${players} плееров, по ${count_req} запросов";
    for (( i = 1; i <= ${players}; i++ )); do
        echo " ${i} users, ${count_req} requests ";
        siege -b -c${i} -r ${count_req} -v -lqa2.txt --mark="${i} users" \
        -H 'X-Player-Secret-Key:579f516a-c7a8-c5bc-05c9-f3ee05cb9a15' \
        -H 'X-Player-Device-Key:b283adad02be6f1d62146c2ec6293d97' \
        -H 'X-Player-Version:win_7_1.1.0.26' \
        "http://qa.fonmix.ru/api/orange/filial/item/music/playlist/info?filial.item.id=158203&client.playlist.id=1020"
    done

else
    echo "Выберите режим тестирования"
fi

