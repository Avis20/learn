package WebApp::Controller::Devel;
use Moose;
use namespace::autoclean;
use uni::perl       qw| :dumper |;
use Encode          qw| encode decode |;
use JSON::XS        qw| decode_json encode_json |;

BEGIN { extends 'Catalyst::Controller' }

__PACKAGE__->config(namespace => 'devel');

sub base :Chained('') :PathPart('devel') :CaptureArgs(0) {
    my ( $self, $c ) = @_;
}

sub index :Chained('base') :PathPart('') :Args(0) {
    my ( $self, $c ) = @_;
    $c->res->body('devel');
}

sub headers :Chained('base') :PathPart('headers') :Args(0) {
    my ( $self, $c ) = @_;
    my $headers = decode('utf8', encode_json({%{ $c->req->headers }}));
    $c->res->body( $headers );
}

sub config2 :Chained('base') :PathPart('config2') :Args(0) {
    my ( $self, $c ) = @_;
    warn dumper $c->config;
    $c->res->body('ok');
}

1;