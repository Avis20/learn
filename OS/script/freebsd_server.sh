#!/bin/bash

VAR=$1;
NAME=$2;
ISO_DIR=$3;

case $VAR in
    "add")

        if [ -z "$NAME" ]; then
            echo "Название обязательно!">&2
            exit 0
        fi

        if [[ !(-f "$ISO_DIR") ]]; then
            echo "iso образ не существует"
            exit 0
        fi

        echo -e '\033[34m---------------------> \033[33mУстановка\033[34m <---------------------\033[0m'

        # Создаем новую vm
        # список уст. машин - vboxmanage list ostypes | egrep ^ID | egrep -i ubuntu
        vboxmanage createvm --name ${NAME} --ostype Ubuntu_64 --register;

        # Настройки
        #                           кол-во ядер,   оперативка       видео память    аудио выкл.     usb выкл.
        vboxmanage modifyvm ${NAME} --cpus 1       --memory 512     --vram 12       --audio none    --usb off;

        #                           сетевой мост    какой адаптер, может менятся!   тип
        vboxmanage modifyvm ${NAME} --nic1 bridged  --bridgeadapter1 enp2s0         --nictype1 82543GC;

        # Создаем hdd 10 гб.
        vboxmanage createhd \
            --filename "/home/avis/VirtualBox VMs/${NAME}/${NAME}.vdi" \
            --size 10000;

        # Цепляем hdd к vm
        vboxmanage storagectl ${NAME} --name "SATA Controller" --add sata;
        vboxmanage storageattach ${NAME} --storagectl "SATA Controller" \
            --port 0 --device 0 --type hdd \
            --medium "/home/avis/VirtualBox VMs/${NAME}/${NAME}.vdi";

        # и iso
        vboxmanage storagectl ${NAME} --name "IDE Controller" --add ide;
        vboxmanage storageattach ${NAME} --storagectl "IDE Controller" \
          --port 1 --device 1 --type dvddrive \
          --medium ${ISO_DIR};

        # грузится с iso
        vboxmanage modifyvm ${NAME} --boot1 dvd;

        # Запускаем
        vboxmanage startvm ${NAME};

    ;;
    *)
        echo "Установка новой виртуальной машины FreeBSD"
        echo "Нужно передать в аргументах:"
        echo "1 Действие - add"
        echo "2 как будет называться машина"
        echo "3 путь до iso образа"
        exit 0
    ;;
esac 


# # # говорим грузится с hdd
# # # vboxmanage modifyvm ${NAME} --boot1 disk;

# # # отцепляем iso
# # # vboxmanage storageattach ${NAME} --storagectl "IDE Controller" --port 1 --device 0 --medium none;

# # и снова запускаем
# # nohup VBoxHeadless --startvm ${NAME} &


# # vboxmanage createmedium disk --filename "/home/avis/VirtualBox VMs/${NAME}/${NAME}.vdi" --size 128 --format VDI --variant Fixed;
# # vboxmanage storageattach ${NAME} --storagectl SAS --port 3 --type hdd --medium "/home/avis/VirtualBox VMs/${NAME}/${NAME}.vdi";


# # vboxmanage createmedium disk --filename "/home/avis/VirtualBox VMs/${NAME}/test.vdi" --size 128 --format VDI --variant Fixed;


#  # sudo apt-get install openssh-server;