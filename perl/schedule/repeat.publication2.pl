#!/usr/local/bin/perl

=head 

    Скрипт повтора публикаций по переданным id-шникам публикаций
    Входящие параметры: task_id - id задания
    В параметрах задания: list_publication - список публикаций, list_playlist - список подборок которые обновились(опционально)

=cut

use lib::abs        qw| ../../lib |;
use uni::perl       qw| :dumper |;
use JSON::XS        qw| decode_json encode_json |;
use URI;
use LWP::UserAgent;
use HTTP::Headers;

use External::Config2;
use FonMix::Plugin::Utils;
use FonMix::Schema::MainDB;
use FonMix::Model::Store;

use constant {
    DEBUG               => $ENV{DEBUG} // 0,
    URI_GET_PLAYLIST    => '/api/yellow/base/playlist/item/get',
};

my $config = External::Config2->new( project => 'fonmix' );
$config->{host} = $ENV{HOST} if DEBUG == 2;

my $schema = FonMix::Schema::MainDB->connect( @{$config->{'Model::MainDB'}->{'connect_info'}} );
my $dbh = $schema->storage->dbh;

my $args = FonMix::Plugin::Utils->args(
    task_id => {
        required => 1,
        type     => 'integer',
    }
);
my $task_id = $args->{task_id};
my $task;

# берём в работу 
eval {
    $schema->txn_do(sub {
        $task = $dbh->selectrow_hashref(
            'SELECT * FROM task.items WHERE id = ? AND status_id = 11 FOR UPDATE NOWAIT', {},
            $task_id,
        );
        die 'Tack "'.$task_id.'" with status_id=11 not found!' unless $task;
        $dbh->do(
            'UPDATE task.items SET status_id = 12 WHERE id = ?', {},
            $task_id,
        ) unless DEBUG;
    });
};
if ($@) {
    die;
}

# получаем входящие параметры
my $params = eval { decode_json( $task->{json_params} ) };
if ($@){
    die fail("Error to parse task input params. task_id: [$task_id] $@");
}

# required params
die fail("Must be at least one publication. task_id: [$task_id]") unless @{ $params->{list_publication} || [] };

my $publication = $dbh->selectrow_hashref(
    'SELECT json_schedule, schedule_id, list_update_playlist
     FROM schedule.publications
     WHERE id = ?', {},
    $params->{list_publication}->[0],
);

die fail("Can't get schedule.publication on id - [$params->{filial_list}->[0]], task_id - [$task_id]")
  unless $publication;

my $schedule = eval { decode_json( $publication->{json_schedule} ) };
if ($@){
    die fail("Error to parse schedule.publication json_schedule. id - [$publication->{id}], task_id: ['$task_id'] $@");
}

my $result_json = {
    success => 1,
    data    => {
        schedule    => $schedule,
        playlists   => [],
    },
};

my %list_related;
while ( my ( $day, $blocks ) = each( %{ $schedule } ) ) {
    foreach my $block ( @{ $blocks } ) {
        foreach my $playlist_id ( @{ $block->{id} } ){
            next if exists $list_related{$playlist_id};
            my $json_playlist = get_meta_for_playlist_from_fonmix( $playlist_id );
            if ( $json_playlist && ref $json_playlist eq 'HASH' ) {
                push @{ $result_json->{data}->{playlists} }, $json_playlist;
                $list_related{$playlist_id} = 1;
            } else {
                die fail("Can't get info about playlist. playlist - [$playlist_id]".( $json_playlist ? ": $json_playlist" : '' ));
            }
        }
    }
}

# my $file_name = "repeat.schedule.publication.task_id_${task_id}.time_".time;
# my $store_source_data = FonMix::Plugin::Utils->create_zip_archive( $file_name, $result_json, 1 ); # upload_to_store
# die fail( $store_source_data ) unless $store_source_data eq 'HASH' || exists $store_source_data->{uri};

my $store_source_data = {
    uri => "http://store3.qa.prototypes.ru/source/file/5ad1e212-0000-0000-0000-000081eb2391.src/repeat_schedule_publication_task_id_114_time_1523704338.zip",
};

my $command_values = encode_json( { uri => $store_source_data->{uri} } );
my $action = $dbh->selectrow_hashref(
    'SELECT id FROM device.actions WHERE alias = \'update_audio_schedule\'', {},
);
=head
my $txn_guard = $schema->txn_scope_guard();
foreach my $filial_key ( @{ $params->{list_filial_key} || [] } ){

    my $action_queue = $dbh->selectrow_hashref(
        'INSERT INTO device.action_queue ( action_id, key, user_id, values ) VALUES ( ?, ?, ?, ? ) RETURNING *', {},
        $action->{id},
        $filial_key,
        25, # системный пользователь
        $command_values,
    );

    my $publication = $dbh->do(
        'INSERT INTO schedule.publications (process_status, process_message, user_id, schedule_id, json_schedule, device_action_queue_id, note) VALUES ( ?, ?, ?, ?, ?, ?, ? ) RETURNING *', {},
        13, # выполнена
        'OK',
        25,
        $publication->{item_id},
        $publication->{json_schedule},
        $action_queue->{id},
        ( scalar @{ $publication->{list_update_playlist} } ? 'Были обновлены следующие подборки: ' . join ', ', @{ $publication->{list_update_playlist} } : '' ),
    );
}
$txn_guard->commit();

=cut
exit;

# получение меты по подборке с сервера по API
sub get_meta_for_playlist_from_fonmix {
    my $playlist_id = shift;
    my $json_playlist;

    my %params = (
        'place' => '25',
        'with'  => 'list_content_meta',
        'id'    => $playlist_id,
    );
    my $url = URI->new( 'http://' . $config->{host} . URI_GET_PLAYLIST );
    $url->query_form( %params );

    $json_playlist = _get_from_fonmix( $url );

    return $json_playlist->{error} unless $json_playlist->{success};
    return $json_playlist->{data}->{item};
}

# выполняет запрос на fonmix
sub _get_from_fonmix {
    my $url = shift;

    my $ua = LWP::UserAgent->new();
    my $h  = HTTP::Headers->new();

    $h->header( 'X-Secret-Key' => $config->{FonMix}{secret_key} );
    $ua->default_headers( $h );

    my $response = $ua->get( $url );
    my $res_hash = eval { decode_json( $response->decoded_content ) };
    if ( $@ ) {
        fail( $@ . "\n" . $response->decoded_content );
        return {};
    }

    return $res_hash;
}

sub fail {
    my $message = shift;

    $schema->storage->dbh->do(
        'UPDATE task.items SET status_id = -13, message = ?  WHERE id = ?',
        {}, $message, $task_id, ) unless DEBUG;

    return $message;
}
