#!/bin/bash

name=$1;
iso_path=$2;

# Создаем новую vm
# список уст. машин - vboxmanage list ostypes | egrep ^ID | egrep -i ubuntu
vboxmanage createvm --name ${name} --ostype Ubuntu_64 --register;

# Настройки
#                           кол-во ядер,   оперативка       видео память    аудио выкл.     usb выкл.
vboxmanage modifyvm ${name} --cpus 1       --memory 512     --vram 12       --audio none    --usb off;

#                           сетевой мост    какой адаптер, может менятся!   тип
vboxmanage modifyvm ${name} --nic1 bridged  --bridgeadapter1 enp2s0         --nictype1 Am79C973;

# Создаем hdd 10 гб.
vboxmanage createhd \
    --filename "/home/avis/VirtualBox VMs/${name}/${name}.vdi" \
    --size 10000;

# Цепляем hdd к vm
vboxmanage storagectl ${name} --name "SATA Controller" --add sata;
vboxmanage storageattach ${name} --storagectl "SATA Controller" \
    --port 0 --device 0 --type hdd \
    --medium "/home/avis/VirtualBox VMs/${name}/${name}.vdi";

# и iso
vboxmanage storagectl ${name} --name "IDE Controller" --add ide;
vboxmanage storageattach ${name} --storagectl "IDE Controller" \
  --port 1 --device 1 --type dvddrive \
  --medium ${iso_path};

# грузится с iso
vboxmanage modifyvm ${name} --boot1 dvd;

# Запускаем
vboxmanage startvm ${name};

# говорим грузится с hdd
# vboxmanage modifyvm ${name} --boot1 disk;

# отцепляем iso
# vboxmanage storageattach ${name} --storagectl "IDE Controller" --port 1 --device 0 --medium none;

# и снова запускаем
# nohup VBoxHeadless --startvm ${name} &


# vboxmanage createmedium disk --filename "/home/avis/VirtualBox VMs/${name}/${name}.vdi" --size 128 --format VDI --variant Fixed;
# vboxmanage storageattach ${name} --storagectl SAS --port 3 --type hdd --medium "/home/avis/VirtualBox VMs/${name}/${name}.vdi";


# vboxmanage createmedium disk --filename "/home/avis/VirtualBox VMs/${name}/test.vdi" --size 128 --format VDI --variant Fixed;


 # sudo apt-get install openssh-server;