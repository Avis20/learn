#!/usr/bin/env perl

use POSIX qw(:sys_wait_h);
$| = 1;

my ($r, $w);
pipe($r, $w);
if (my $pid = fork()){
    close($r);
    print $w $_ for 1..5;
    close($w);
} else {
    die "Can't fork $!" unless defined $pid;
    close($w);
    print $_ while (<$r>);
    close($r);
    exit;
}

exit;