
integer array(5)                 ! обьявление массива из 5 эл.
integer array1(5)                ! обьявление массива из 5 эл.

! обьявление массива из 10 эл.
real arr1(-4, 5), arr2(0, 9), arr3(1, 10), arr4(10)

! Конструктор массива
real :: array2(5) = (/ 1.1, 1.2, 1.3, 1.4, 1.5 /)

! Конструктор массива через цикл
real :: array3(10) = (/(i, i=1, 10, 1)/)

array2 = 2.0 * array2;

print *, array1
print *, array2
print *, array3

array(1) = 12                   ! привсвоить 1 эл. 12
array(2) = 1; array(3) = 45

print *, 'array(1) - ', array(1) ! вывести 1 эл
print *, 'array - ', array    ! вывести все эл. массива

end