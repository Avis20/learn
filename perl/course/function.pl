#!/usr/bin/perl

use uni::perl       qw| :dumper |;

my $test1 = test();
my @test2 = test();

# warn $test1;
# warn @test2;
# warn test();

sub test {
    return wantarray ? (1,2,3) : 2;
}

my $test1 = sub { say "@_"; shift; };
sub why { $test1->(@_); say "@_"; };

why(1,2,3,4);

=head
my $str = 'test';
warn substr($str, 1, 2) = 'yy';
warn $str;
=cut

exit;