package WebChat::Controller::API;
use Moose;
use namespace::autoclean;

use uni::perl qw| :dumper |;
use JSON::XS;

BEGIN { extends 'Catalyst::Controller' }

__PACKAGE__->config(namespace => 'api');

sub base: Chained PathPart('api') CaptureArgs(0) {
    my ($self, $c) = @_;
    my $client_id = $c->request->cookies->{client_id} && $c->request->cookies->{client_id}->value ?
        $c->request->cookies->{client_id}->value : 0;
    $c->detach('/default') unless $client_id;
    $c->stash->{channels} = {
        support => 'support_notify_channel',
        client  => 'client_channel_'.$client_id,
    };
    $c->stash->{user} = {
        id  => $client_id,
    };
}

sub index: Chained('base') PathPart('') Args(0) {
    my ($self, $c) = @_;
    $c->stash->{json_data} = { success => 1 };
}

sub client: Chained('base') PathPart('client') CaptureArgs(0) {}

sub csend: Chained('client') PathPart('send') Args(0) {
    my ($self, $c) = @_;
    if ( $c->req->param('message') ) {
        my $history = decode_json($c->cache->get('test') || '{ "messages": [] }');
        my $block = {
            msg     => $c->req->param('message'),
            user    => $c->stash->{user}->{id},
            from    => 'client',
        };
        push @{$history->{messages}}, $block;
        $c->cache->set('test', encode_json($history) );
        my $command =  "curl -s -v -X POST 'http://webchat.galaev.me/pub?id=".$c->stash->{channels}->{client}."' -d '".(encode_json($block))."'";
        my $res = system($command);
        my $for_stp = {
            msg     => $c->req->param('message'),
            type    => 'message',
            user    => $c->stash->{user}->{id},
            link    => $c->uri_for('/support/'.$c->stash->{channels}->{client})->as_string,
        };
        if ( $history->{support_id} ) {
            $c->stash->{channels}->{support} .= '_'.$history->{support_id};
        }
        $command =  "curl -s -v -X POST 'http://webchat.galaev.me/pub?id=".$c->stash->{channels}->{support}."' -d '".(encode_json($for_stp))."'";
        $res = system($command);
        return $c->stash->{json_data} = { success => 1 };
    }
    $c->stash->{json_data} = { success => 0 };
}
sub cread: Chained('client') PathPart('messages') Args(0) {
    my ($self, $c) = @_;
    my $history = decode_json($c->cache->get('test') || '{ "messages": [] }');
    $c->stash->{json_data} = { success=> 1, %$history };
}
sub close: Chained('client') PathPart('close') Args(0) {
    my ($self, $c) = @_;
    my $history = decode_json($c->cache->get('test') || '{ "messages": [] }');
    $c->cache->delete('test');

    my $for_stp = {
            msg     => 'Client close channel',
            type    => 'notice',
            user    => $c->stash->{user}->{id},
    };
    if ( $history->{support_id} ) {
        $c->stash->{channels}->{support} .= '_'.$history->{support_id};
    }
    my $command =  "curl -s -v -X POST 'http://webchat.galaev.me/pub?id=".$c->stash->{channels}->{support}."' -d '".(encode_json($for_stp))."'";
    my $res = system($command);

    $command =  "curl -s -v -X DELETE 'http://webchat.galaev.me/pub?id=".$c->stash->{channels}->{client}."'";
    $res = system($command);
    $c->stash->{json_data} = { success=> 1 };
}

sub support: Chained('base') PathPart('support') CaptureArgs(0) {
    my ($self, $c) = @_;
    $c->stash->{channels}->{client} = $c->req->param('channel');
}

sub ssend: Chained('support') PathPart('send') Args(0) {
    my ($self, $c) = @_;
    if ( $c->req->param('message') ) {
        my $history = decode_json($c->cache->get('test') || '{ "messages": [] }');
        my $block = {
            msg     => $c->req->param('message'),
            user    => $c->stash->{user}->{id},
            from    => 'support',
        };
        push @{$history->{messages}}, $block;
        $history->{support_id} = $c->stash->{user}->{id};
        $c->cache->set('test', encode_json($history) );
        my $command =  "curl -s -v -X POST 'http://webchat.galaev.me/pub?id=".$c->stash->{channels}->{client}."' -d '".(encode_json($block))."'";
        my $res = system($command);

        my $for_stp = {
            msg     => 'Support answer for '.$c->stash->{channels}->{client},
            user    => $c->stash->{user}->{id},
            type    => 'notice',
        };
        my $command =  "curl -s -v -X POST 'http://webchat.galaev.me/pub?id=".$c->stash->{channels}->{support}."' -d '".(encode_json($for_stp))."'";
        my $res = system($command);
        return $c->stash->{json_data} = { success => 1 };
    }
    $c->stash->{json_data} = { success => 0 };
}
sub sread: Chained('support') PathPart('history') Args(0) {
    my ($self, $c) = @_;
    my $history = decode_json($c->cache->get('test') || '{ "messages": [] }');
    $c->stash->{json_data} = { success=> 1, %$history };
}

sub end : ActionClass('RenderView') {}

__PACKAGE__->meta->make_immutable;

1;
