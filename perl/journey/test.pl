#!/usr/local/bin/perl

use uni::perl       qw| :dumper |;
use Encode          qw| encode decode |;
use JSON::XS        qw| decode_json encode_json |;
use UUID::Generator::PurePerl;
use Archive::Zip    qw| :ERROR_CODES :CONSTANTS |;
$Archive::Zip::UNICODE = 1;

my $str = '{"tags": [], "lon": 1.7976931348623157E+308, "text": "", "music_artist": "", "preview_text": "", "date_modified": 0, "mood": 0, "date_journal": 0, "photos": [], "id": "1529387288194-3fda201b49047874", "address": "", "music_title": "", "weather": {"id": -1, "degree_c": 1.7976931348623157E+308, "description": "", "icon": "", "place": ""}, "lat": 1.7976931348623157E+308 }';

my $ug = UUID::Generator::PurePerl->new();

my $template = decode_json( encode('utf8', $str ) );

open my $fh, './data.json' or die "Can't open ./data1.json: $!";
my $json = <$fh>;
my $hash = decode_json( encode('utf8', $json ) );

my $zip_name = 'journey-single3.zip';
my $zip      = Archive::Zip->new();

for ( @{ $hash->{marks} } ){
    next unless $_->{comment};
    my $time = unix_time( $_->{date} . "T" . $_->{time} );

    my $data = $template;
    $data->{text} = $data->{preview_text} = $_->{comment};
    chomp($time);
    $data->{date_modified} = $data->{date_journal} = $time + 0;
    my $uuid1 = $ug->generate_v1()->as_string();
    $uuid1 = substr( join( '', split( '-', $uuid1 ) ), 3);
    $uuid1 = join '-', unpack '(a15)*', $uuid1;

    $data->{id} = $uuid1;
    $data = encode_json( $data );
    $zip->addString( $data, $uuid1.'.json' );
}

unless ( $zip && $zip->writeToFileNamed( "./$zip_name" ) == AZ_OK ) {
    fail( "Не удалось создать архив - ./$zip_name" );
    die 'write error';
}

close $fh;

sub unix_time {
    my $datetime = shift;
    my $utime = qx{ date -d $datetime +%s%N | cut -b1-13 };
    return $utime;
}

exit;
