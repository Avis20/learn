package FonMix::Plugin::OTRS;
use uni::perl qw|:dumper|;

use FonMix::Plugin::OTRS::Object;

sub otrs {
    my $c = shift;
    $c->stash->{_otrs} ||= FonMix::Plugin::OTRS::Object->new(context => $c);
}

1;
