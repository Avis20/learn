#!/usr/bin/env perl

# use uni::perl       qw| :dumper |;



# open my $fh, 'Такого файла не существует!' or die $!;
# open my $fh, 'Такого файла не существует!' || die $!;

=head
use strict;
use Digest::MD5 qw| md5_hex |;

$\ = "\n";
my $data = '';
my $data_size = 1024;

open my $fh, '<:raw', 'a.out' or die $!;

until ( eof($fh) ){
    read($fh, $data, $data_size) == $data_size
      or die "Неверный формат";
    print md5_hex($data);
}
close($fh);

=head
die "Not root" if $<;
print "You are ROOOT\n";

open my $fh, 'test.txt' or die $!;
warn eof($fh);
my @test = <$fh>;
warn eof($fh);

my $data = "Test\n";
read($fh, $data, 2);
warn <$fh>;
open my $fh, '>', 'test.txt' or die $!;
syswrite($fh, $data, length($data));

my $line = <DATA>;
warn $line;

__DATA__
Это текст который игнорирует перл, но может прочитать ;)

=head
open my $fh1, '>', 'file1.txt' or die $!;
open my $fh2, '>', 'file2.txt' or die $!;

print "Написать в STDOUT\n";
select($fh1);
print "Написать в файл file1.txt\n";
my $old_fh = select($fh2); # можно выбрать и записать в переменную, причем сохраниться GLOB
print "Написать в файл file2.txt\n";
select(STDOUT);
print "Написать опять в STDOUT\n";
select($old_fh); # востановить предыдущий файлхендлер
print "Написать опять в файл file2.txt\n";

close $fh2;
close $fh1;

=head
open my $fh, '>', 'test.txt';
my $var = 'test';
print $var; # Пишет в STDOUT
print STDERR "ALARM!"; # Можно указывать явно поток
print $fh $var; # Пишет в файл
print {$fh} $var; # Можно и так

my $input = <$fh>; # Первую строку
say $input;
my @input = <$fh>; # Или все строки
say @input;


open my $fh_utf, '>:encoding(UTF-8)', 'utf8.txt';

while ( <$fh_cp> ){
    print $fh_utf $_;
}

close($fh_utf);
close($fh_cp);

open my $fh, '<:encoding', 'unicode.txt';
warn <$fh>;
close $fh;

open my $fh, 'unicode.txt:unicode';
warn <$fh>;
open my $fifo, '<', 'test.pipe';
while(<$fifo>){
    print "Get: $_";
}
close $fifo;
{
    local $SIG{INT} = 'IGNORE';

    sleep 3;
}

warn "NOW";

sleep 10;

$SIG{CHLD} = sub {
    while (my $pid = waitpid(-1, ))
}
=cut

exit;