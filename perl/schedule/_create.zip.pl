#!/usr/local/bin/perl

=head
    Приватный скрипт для создания архива

=cut

use lib::abs        qw| ../../lib |;
use uni::perl       qw| :dumper |;
use Archive::Zip    qw| :ERROR_CODES :CONSTANTS |;
$Archive::Zip::UNICODE = 1;

my $tmp_dir;
if ( $ENV{DEBUG} ) {
    $tmp_dir = $ENV{HOME}.'/tmp';
} else {
    $tmp_dir = '/spool/tmp';
}

sub create_zip_archive {
    my ( $name, $content, $upload_to_store ) = @_;

    my $zip_name = "$tmp_dir/$name.zip";
    my $zip = Archive::Zip->new();
    $zip->addString( encode_json $result_json, $file_name . ".json" );

    unless ( $zip->writeToFileNamed( "$zip_name" ) == AZ_OK ) {
        die fail( "Can't create zip archive - $zip_name" );
    }

    # загружаем созданный файл в хранилище
    my $model_store = FonMix::Model::Store->new();
    my $store_source_data = $model_store->add_item_with_file( { file => $zip_name } );

    unlink $zip_name;

    unless ( ref $store_source_data eq 'HASH' && $store_source_data->{uri} ) {
        die fail( 'Не удалось сохранить файл с обновлением на Store' );
    }


}

exit;