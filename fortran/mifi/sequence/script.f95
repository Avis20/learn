implicit none

real :: i

print *, (/(i, i=1, 10, 0.1)/)

! real array(5)

! array = 1.0

! print *, array

! ! Using real step size in loops
! implicit none

! ! integer, parameter :: n = 1
! integer :: i, n
! real, parameter :: step = 0.1
! real :: x
! real :: array(5)
! ! real :: array(n) = (/(i, i=1, 5, 0.1)/)

! 10 format(i2 a6 f3.1)

! n = 10
! write (*,*) 'Using DO loop with control variable:'
! do i=1, n+1
!   write(*,10) i, ". x = ", x
!   x = i*step
!   array(i) = x
! end do

! print *, array

end