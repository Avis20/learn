sub propis 
{ 
  my $summa = shift @_; 
  $summa=sprintf("%20.2f",$summa*1); 
  $summa =~ s/\s//g; 
  my ($v, $value, $kop, $v1); 
  my @nam_9=("МИЛЛИАРД ","МИЛЛИАРДА ","МИЛЛИАРДОВ "); 
  my @nam_6=("МИЛЛИОН ","МИЛЛИОНА ","МИЛЛИОНОВ "); 
  my @nam_3=("ТЫСЯЧА ","ТЫСЯЧИ ","ТЫСЯЧ "); 
  my @nam_1=("РУБЛЬ ","РУБЛЯ ","РУБЛЕЙ "); 
  my %namf=(0,\@nam_9,1,\@nam_6,2,\@nam_3,3,\@nam_1); 
  my @xx = ("","ОДИН","ДВА","ТРИ","ЧЕТЫРЕ","ПЯТЬ","ШЕСТЬ","СЕМЬ","ВОСЕМЬ","ДЕВЯТЬ","ДЕСЯТЬ"); 
  for(11..19) { 
    $xx[$_] = (($_ >=14) ? substr($xx[$_-10],0,length($xx[$_-10])-1) : $xx[$_-10])."НАДЦАТЬ"; 
  } 
  $xx[12]="ДВЕНАДЦАТЬ"; 
  my @yy=("",$xx[10], "ДВАДЦАТЬ","ТРИДЦАТЬ","СОРОК"); 
  for(5..8) {  $yy[$_]=$xx[$_]."ДЕСЯТ";  } 
  $yy[9]="ДЕВЯНОСТО"; 
  $yy[10]="СТО"; 
  my @zz=("","СТО","ДВЕСТИ","ТРИСТА","ЧЕТЫРЕСТА"); 
  for(5..9) { $zz[$_]=$xx[$_]."СОТ";  } 
  ($value,$kop) = split(/\./,$summa); 
  return $summa if($value>=1000000000000); 
  return "" if($summa == 0); 
  my $mess=''; 
  if($value == 0) { $mess = "0 РУБЛЕЙ "; } 
  my $divisor=1000000000; 
  my $part=0;   
  while($part<4) { 
    my $i; 
    if($value>=$divisor) { 
      if($part == 2) {   # тысячи 
        $xx[1]="ОДНА"; $xx[2]="ДВЕ";   
      } else {   
        $xx[1]="ОДИН"; $xx[2]="ДВА";   
      } 
      $v=int($value/$divisor); 
      $value-=($v*$divisor); 
      if($v>100)  {  $v1=int($v/100);  $mess.=$zz[$v1].' ';   $v-=$v1*100; } 
      if($v>=20)  {  $v1=int($v/10);   $mess.=$yy[$v1].' ';   $v-=$v1*10;  } 
      if($v>0)    {  $mess.=$xx[$v].' '; } 
      if($v==1) { 
        $i=0; 
      } elsif($v>=2 && $v<=4) { 
        $i=1; 
      } else { 
        $i=2; 
      } 
      $mess.=$namf{$part}->[$i]; 
    } 
    $divisor/=1000; 
    $part++; 
  } 
  $mess.=$kop; 
  $v = int($kop%10); 
  if( $v==1 ) { 
    $mess.=' КОПЕЙКА'; 
  } elsif($v>=2 && $v<=4) { 
    $mess.=' КОПЕЙКИ'; 
  } else { 
    $mess.=' КОПЕЕК'; 
  } 
  return $mess; 
} 