#!/usr/bin/perl

=head
    Сервер на AnyEvent - не доделал
=cut

use uni::perl qw/:dumper/;
use AnyEvent;

my $cv = AnyEvent::condvar;

my $alive_timer = AnyEvent->timer(
    after => 2,
    interval => 1,
    cb => sub {
        say "server alive";
    },
);

$cv->recv;

exit;
