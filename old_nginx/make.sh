#!/bin/bash

sudo ln -f ./nginx.conf /etc/nginx/
sudo ln -f ./proxy.conf /etc/nginx/

sudo rm /etc/nginx/sites-enabled/default

sudo ln -f ./default.conf /etc/nginx/sites-enabled/
