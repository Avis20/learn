#define PERL_NO_GET_CONTEXT
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "ppport.h"

#include "const-c.inc"

MODULE = Local::Base58::XS		PACKAGE = Local::Base58::XS		

INCLUDE: const-xs.inc
