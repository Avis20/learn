#!/usr/bin/perl 
use strict;
use warnings;
use Fcntl qw(:flock);
# https://confluence.id-network.ru/pages/viewpage.action?pageId=26968859
my $dir_num = $ARGV[ 0 ];
die "dir number is absent" unless defined $dir_num;
my $dir_in        = '/usr/home/george/DN/Music/in_' . $dir_num;
my $dir_out       = '/usr/home/george/DN/Music/out_' . $dir_num;
die "folder is absent" unless -d $dir_in && -d $dir_out;
my $dir_tmp       = '/usr/home/george/DN/Music/tmp/';
my $dir_plugins   = '../plugins/';
my $dir_mrswatson = '/usr/home/george/DN/MrW/Windows/';
my $RmsIt         = 6;
my $converter_lock=$dir_tmp.'lock.'.$dir_num;
open(my $lck, ">", $converter_lock) or die "lock $!";
flock($lck, LOCK_EX) or die "flock() failed for: $!";

opendir( my $dh, $dir_in ) || die;
while ( readdir $dh ) {
    next if $_ =~ /^\.+$/;
    next unless $_ =~ /\.mp3/i;
    my $file_orig = $_;
    ( my $file_wav = $file_orig ) =~ s/\.mp3/\.wav/i;
    ( my $file_txt = $file_orig ) =~ s/\.mp3/\.txt/i;
    my $sox_mp3_to_wav = `sox $dir_in/$file_orig ${dir_tmp}$file_wav 2>&1`;
    open( my $fh, ">", "${dir_tmp}$file_txt" ) or die "cannot open: $!";
    print $fh 'proccess';
    close $fh;

    #RMS Pk dB      -6.24     -6.25     -6.24
    #Length s     158.145
    my $sox_stats = `sox ${dir_tmp}$file_wav -n stats 2>&1`;

    my $bitrate;
    my ( $RMS_Pk ) = $sox_stats =~ /RMS Pk dB\s+(\S+)\s+/m;
    my ( $Length ) = $sox_stats =~ /Length s\s+(\S+)\s+/m;

    #ffprobe -show_format -hide_banner
    #Stream #0:0: Audio: mp3, 44100 Hz, stereo, s16p, 320 kb/s
    #bit_rate=320052

    my $ffprobe_show_format = `ffprobe -show_format -hide_banner $dir_in/$file_orig 2>&1`;
    my ( $mono_stereo )     = $ffprobe_show_format =~ /Stream.+?Audio:.+?(stereo|mono)/m;
    my ( $bit_rate )        = $ffprobe_show_format =~ /bit_rate=(\d+)/m;
    unlink "$dir_tmp/$file_txt";
    my $rms = int( ( abs $RMS_Pk ) + .5 );
    open( my $fh_rms, ">>", "${dir_tmp}rms.txt" ) or die "cannot open: $!";
    print $fh_rms $rms;
    close $fh_rms;

    if ( $rms > $RmsIt ) {
        $rms = $rms - $RmsIt;
    } # if ...
    else {
        $rms = 0;
    } # else [ if ...]
    $rms++;
    my $mrswatson =
`wine ${dir_mrswatson}mrswatson.exe -p "${dir_plugins}LoudMax.dll,${dir_plugins}LoudMax_${rms}db.fxp;${dir_plugins}LoudMax.dll,${dir_plugins}LoudMax_-1db.fxp" -i "${dir_tmp}${file_wav}" -o "${dir_tmp}tmp.${file_wav}" 2>&1`;

    my $sox_silence =
`sox ${dir_tmp}tmp.${file_wav} ${dir_tmp}silence.${file_wav} silence 1 0.1 6%% reverse silence 1 0.1 10%% reverse 2>&1`;
    my $sox_fade = `sox ${dir_tmp}silence.${file_wav} ${dir_tmp}fade.${file_wav} fade t 1 0 1 2>&1`;
    my $twolame_wav_to_mp3 = `twolame ${dir_tmp}fade.${file_wav} $dir_out/$file_orig 2>&1`;

#my $ffmpeg_wav_to_mp3=`ffmpeg -i ${dir_tmp}fade.${file_wav} -codec:a libmp3lame -ab 320k -aq 0 $dir_out/$file_orig 2>&1`;
    unlink "${dir_tmp}fade.${file_wav}";
    unlink "${dir_tmp}silence.${file_wav}";
    unlink "${dir_tmp}tmp.${file_wav}";
    open( my $fh_final_status, ">", "${dir_out}/${file_txt}" ) or die "cannot open: $!";
    if ( -f "$dir_out/$file_orig" ) {
        print $fh_final_status 'converted';
    } # if ...
    else {
        print $fh_final_status 'error';
    } # else [ if ...]
    close $fh_final_status;
    #last;

} # while ...
closedir $dh;
