package MyPackage;

sub new {
    my ( $class, %params ) = @_;
    my $self = {};
    bless $self, $class;
    return $self;
}

sub import {
    my $self = shift;
    my $caller = caller;

    for my $func ( qw( test1 test2 test3 ) ) {
        no strict 'refs';
        *{$caller."::".$func} = \&$func;
    }

    warn $caller;
}

sub test1 { 1 };
sub test2 { 2 };
sub test3 { 3 };

1;