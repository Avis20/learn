#!/usr/bin/perl
use uni::perl qw/:dumper/;
use AnyEvent;

my $cv = AnyEvent->condvar;

# =head 1 раз после 1 секунды ожидания 
my $watcher = AnyEvent->timer (
    after   => 1,     # время наступления события
    cb      => sub {  # вызов калбека 
        say "hi";
        $cv->send;
    }
);
=cut

=head Если нужен бесконечный цикл
my $watcher = AnyEvent->timer (
    after   => 0,     # время наступления события, 0 - сразу
    interval => 1,
    cb      => sub {  # вызов калбека 
        say "hi";
    }
);
=cut

$cv->recv;

exit;