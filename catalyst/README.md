# Catalyst #


## Установка и инициализация ##

```
#!bash

# скрипт запуска инициализации
sudo apt-get install libcatalyst-perl

# библиотеки и сам catalyst
perl -MCPAN -e 'install Catalyst::Devel'

# Если проблемы(Installing Starman failed), то попробовать установить принудительно
# cpanp -i --force Catalyst::Devel

# Инит WebApp
catalyst.pl WebApp

```

## Запуск встроенного однопоточного сервера ##

```
#!bash

perl WebApp/script/server.pl -p 3001 -rd
# где, 
# -p <порт> который слушает сервер
# -r релоад при изменении файлов
# -d ХЗ

```
