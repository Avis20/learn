#!/usr/bin/env perl

use uni::perl       qw| :dumper |;

# Сложные структуры

my $href = {
    'test'  => 1,
    's'     => 'string',
};

$href->{array}[7] = 'seven';

# Последний индекс в массиве.
# ТОЛЬКО ДЛЯ arrayref!
warn 'last index - '.$#{ $href->{array} };
warn 'size - '.@{ $href->{array} };

$href->{s}{error} = "what?";
warn $href->{s}{error};

nouse strict 'refs';
warn $string{error};


