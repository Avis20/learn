

# Links #

* https://www.8host.com/blog/ustanovka-i-nastrojka-sphinx-v-ubuntu-14-04-2/
* https://ph0en1x.net/32-sphinx-search-server-engine-on-linux-setup.html - сборка сфинкса

# 1. Подготавливаем базу #


```
#!bash

mysql> CREATE TABLE movies (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, genre TEXT, year INTEGER, rating REAL);

```





# indextool #

## --dumpdict <index> - сдампить индекс ##

```
#!bash

indextool --dumpdict test1

using config file '/etc/sphinxsearch/sphinx.conf'...
dumping dictionary for index 'test1'...
keyword,docs,hits,offset
1,2,2,103
11,4,4,185
12,4,8,76
2,2,2,11
2017,4,4,20
30,4,4,136
56,4,4,37
also,1,1,54
another,1,2,166
...

```


