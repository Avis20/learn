#!/usr/bin/perl
use uni::perl qw/:dumper/;

use Archive::Zip qw( :ERROR_CODES :CONSTANTS );

my $zip = Archive::Zip->new();

my $string_member = $zip->addString( 'This is a test', 'stringMember.txt' );

# Save the Zip file
unless ( $zip->writeToFileNamed('someZip.zip') == AZ_OK ) {
   die 'write error';
}

# $z->close;

exit;