# FreeBSD 11.1


### Создание виртуальной машины (через интерфейс)

1. Создать -> FreeBSD -> Далее -> Далее -> Готово
2. Установить 10 ГБ HDD
3. Настройки:  
    a. Носители -> Атрибуты -> Оптический привод -> <путь до iso>  
    b. Сеть:  
        * Включить сетевой адаптер  
        * Тип подключения - Сетевой мост  
        * Имя - еcли Wi-Fi, то wlan0, если провод, то eth  
        * Дополнительно -> Тип адаптера -> Intel PRO 1000 T Server  
4. Запустить

### Создание виртуальной машины (скрипт)
```
bash /home/avis/develop/learn/OS/script/freebsd_server.sh add FreeBSD /home/avis/Загрузки/FreeBSD-11.1-RELEASE-amd64-disc1.iso
```

### Установка системы
1. Ввести хост машины - freebsd11
2. Distribution selected. Выбрать только - lib32
3. Разметка диска:
    * Auto (UFS)
    * GPT - GUID Partiotion Table
    * Finish -> commit

4. пароль руту - avis20
5. Настройки интернета
    * IPv4 - yes
    * DHCP - yes
    * IPv6 - no

6. TimeZone:
    * Europe
    * Russian Federation
    * Moskow

8. Добавить другого пользователя:  
    * имя - avis
    * добавить в группу - wheel
    * дальше все по дефолту

9. Указать sshd on
10. Reboot

### Первый запуск

##### инет

Проверяем
```
ping 8.8.8.8

```

Если интернета нет пробуем 1 из вариантов:
------------------------------------------------------------------

1. Говорим какой интерфейс нужно слушать  
media может быть: autoselect, 10baseT/UTP и 100baseTX.
```
ifconfig em0 media 100baseTX 
```

Получаем ip-шник по dhsp
```
dhclient em0
```

Проверяем
```
ping 8.8.8.8
```
------------------------------------------------------------------

2. Проверяем какой у нас по дефолту роутер
```
netstat -r | default
```
через домашнюю машину узнаем ip-шник роутера, так-же как и при установке
```
ip route show | grep default
```
Потом добавиляем его в таблицу маршрутизации на виртуалке
```
route add default 10.20.30.1
```
и вписал его в rc-шник
```
sysrc defaultrouter="10.20.30.1"
```
------------------------------------------------------------------

## Грабли и исправления
* В FreeBSD 10.3 Все по другому!


### Links
* [Дистрибутив](https://download.freebsd.org/ftp/releases/ISO-IMAGES/11.1/FreeBSD-11.1-RELEASE-amd64-disc1.iso)
* [Статья по установке](https://serveradmin.ru/ustanovka-freebsd-11/)

* https://wiki.lissyara.su/wiki/FreeBSD_%D0%A0%D1%83%D0%BA%D0%BE%D0%B2%D0%BE%D0%B4%D1%81%D1%82%D0%B2%D0%BE_%D0%9F%D0%BE%D0%BB%D1%8C%D0%B7%D0%BE%D0%B2%D0%B0%D1%82%D0%B5%D0%BB%D1%8F:_%D0%93%D0%BB%D0%B0%D0%B2%D0%B0_15_Jails_(%D0%9A%D0%BB%D0%B5%D1%82%D0%BA%D0%B8) - Руководство на русском
* https://www.youtube.com/watch?v=Ab30k5mvZok&t=120s
* http://www.site-motor.ru/docs/freebsd/jail.shtml
* https://habrahabr.ru/post/342312/.com.1vvedenie - свежачок

