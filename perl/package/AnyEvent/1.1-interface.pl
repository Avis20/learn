#!/usr/bin/perl
use uni::perl qw/:dumper/;
use AnyEvent;

# Программа ожидает ввода пользователя с STDIN,
# если ввода нет в течении 4.2 секунд — прерывает свою работу
# если ввод есть — выводит введённую строку.

my $done = AnyEvent->condvar;

my ( $w, $timer );

$w = AnyEvent->io(
    fh => \*STDIN,
    poll => 'r',
    cb => sub {
        chmod( my $input = <STDIN> );
        warn "read - $input\n";
        undef $w;
        $done->send;
    },
);

warn $w;

$timer = AnyEvent->timer(
    after => 10,
    cb => sub {
        if ( defined $w ){
            warn "Не было введено ни одного символа в течении 4,2 сек\n";
            undef $w;
            undef $timer;
        }
        $done->send;
    }
);


$done->recv;

