#!/usr/bin/perl
use uni::perl qw/:dumper/;
use Archive::Zip    qw| :ERROR_CODES :CONSTANTS |;
use AnyEvent;
use AnyEvent::Util;
$Archive::Zip::UNICODE = 1;

my $arch_dir = $ENV{HOME}.'/tmp';
my $cv = AnyEvent->condvar;
my $zip = Archive::Zip->new();
$cv->begin( sub { shift->send() } );

for (0..5){

    $cv->begin;
    fork_call {
        sleep 1;
        "test $_";
    } sub {

        my $category_string = shift;
        $zip->addString( "$category_string", "$category_string.txt" );
        # warn dumper  $category_string;
        # my $category_name = $category_enum->{alias}->{$category}." за $period ТОП-$top";
        # $zip->addString( $category_string, "$category_name.csv" );
        $cv->end;
    };

    # $zip->addString( 'test', 'test.csv' );
}

$cv->end;
warn $cv->recv;
undef $cv;

my @members = $zip->members();
warn dumper \@members;

unless ( $zip && $zip->writeToFileNamed( "$arch_dir/test.zip" ) == AZ_OK ) {
    die "Не удалось создать архив - $arch_dir/test.zip";
}

exit;