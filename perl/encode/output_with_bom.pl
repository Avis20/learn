#!/usr/bin/perl

open my $fh, '>:utf8','output_with_bom.csv' or die "$!";
print $fh chr(0xFEFF); # U+FEFF is EF BB BF as utf-8
print $fh $_ for <DATA>;
close $fh;

__DATA__
a,1
b,2
©,3
®,4
привет

exit;