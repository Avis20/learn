#!/usr/bin/perl

use lib::abs    qw| ../../lib |;
use uni::perl   qw/:dumper/;
use AnyEvent::Fork;
use Worker;

my $fork = AnyEvent::Fork
            ->new
            ->require('Worker')
            ->run('Worker::just_say_hi', my $cv = AnyEvent->condvar);

my $what = $cv->recv;

exit;