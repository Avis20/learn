#!/usr/bin/perl
use uni::perl qw/:dumper/;
use AnyEvent;
use AnyEvent::Util;

my $cv = AnyEvent->condvar;

# $cv->begin( sub { shift->send() } );
for (my $i = 0; $i < 5; $i++) {
    $cv->begin;
    fork_call {
        sleep 5;
        open my $fh, "</etc/passwd"
         or die "passwd: $!";
        local $/;
        <$fh>
    } sub {
        my ($passwd) = @_;
        warn dumper $passwd;
    };
}

$cv->end;
$cv->recv;
undef $cv;

exit;