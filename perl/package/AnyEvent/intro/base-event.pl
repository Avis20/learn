#!/usr/bin/perl

use uni::perl       qw| :dumper |;
use Encode          qw| encode decode |;
use JSON::XS        qw| decode_json encode_json |;
use AnyEvent;

=head Обычная обработка события
$| = 1;
print "Enter your name>";
my $name = <STDIN>;
=cut

# Асинхронная обработка
$| = 1;
print "Enter your name>";

my $name_read = AnyEvent->condvar;

# Калбек на функцию
# Любой объект переданный в send, вернется в recv после срабатывания события
sub callback { $name_read->send( scalar <STDIN> ) };

my $wait_to_input = AnyEvent->io (
    fh      => \*STDIN,     # какой дескриптор файла проверять
    poll    => "r",         # какое событие ждать("r"ead - чтение)
    cb      => \&callback,
);

say "\nDo something else";

# Повторяем пока переменная существует
my $name = $name_read->recv;

undef $wait_to_input; # Дальше наблюдатель не нужен

print "Your name - ".$name;