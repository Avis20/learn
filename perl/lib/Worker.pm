package Worker;

use uni::perl qw/:dumper/;

our $fh;

sub init {
    
    # Для каждого воркера создаем файлхендлер
    open $fh, '>>', 'file.txt' or die "Can't open file.txt: $!";

    my $q = select($fh);
    $| = 1;
    select($q);

    return;
}

sub work {
    my ( $str ) = @_;


    my $str_length = length($str);

    for (1..$str_length){
        say $fh "$$ $str";
        sleep 1;
    }
    
    return $str, $str_length;
}

sub just_say_hi {
    say "hi";
}

1;