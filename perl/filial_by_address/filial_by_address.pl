#!/usr/bin/perl

=head
    Скрипт поиска точек по адресу
    Аргументы - contract_num - № договора пользователя и file - csv файл со списком точек адресов точек
=cut

use uni::perl       qw| :dumper |;
use lib::abs        qw| ../../lib |;
use Encode;

use FonMix::Schema::MainDB;

my $config = External::Config2->new('project' => "fonmix");
my $schema = FonMix::Schema::MainDB->connect(@{$config->{'Model::MainDB'}->{'connect_info'}});
my $dbh    = $schema->storage->dbh;
die 'Can not connect to DB!' unless $schema->storage->dbh->ping;

my %args = map { /^-(contract_num|file)=(.*)/ ? ($1 => $2) : () } @ARGV;

die "empty -contract_num or -file" unless $args{'contract_num'} || $args{'file'};

open my $fh, $args{'file'} or die "Can't open file with $args{'file'}: $!";
my @addresses = <$fh>;
close $fh;

my $contract = decode( 'utf8', $args{'contract_num'} );

my $filials = $dbh->selectall_hashref(
    "SELECT fil.id, fil.address
     FROM filial.items AS fil 
     LEFT JOIN client.items AS cli ON fil.client_id = cli.id WHERE cli.contract_num = '$contract'", 
    'id',
);

=head
my $filials = {
    158201 => {
        id => 158201,
        address => 'город Москва, улица 1-я Тверская-Ямская, дом 10',
    },
    # 158202 => {
    #     id => 158202,
    #     address => 'город Москва, улица Мясницкая, дом 24/7, стр. 1',
    # },
    # 158203 => {
    #     id => 158203,
    #     address => 'город Москва, улица 1-я Тверская-Ямская, дом 10',
    # },
};
=cut

foreach my $file_adr ( @addresses ){
    foreach ( keys %{ $filials } ){
        my $db_adr = $filials->{$_}->{address};
        my $same = is_same( $db_adr, $file_adr );

        if ( $same == 100 ){
            warn "Полное совпадение адреса \'$file_adr\' : \'$db_adr\'. ID точки - $_";
        } elsif ( $same == 75 ){
            warn "Частичное совпадение адреса \'$file_adr\' : \'$db_adr\'. ID точки - $_";
        }

    }
}

# warn dumper $filials;

=head
open my $fh, 'tracks.csv' or die "Can't open file with tracks: $!";
open my $file_out, "> playlists.csv" or die "Can't open file with tracks: $!";

print $file_out sprintf "%s\t%s\t%s\n", 'UUID трека', 'ID подборки', 'Название подборки';

my @tracks = <$fh>;
my %uniq;
for (@tracks){
    $uniq{$_} = 1;
}

foreach my $track ( keys %uniq ){
    $track =~ s/\n//g;
    my $clean_track = &clean_uuid( $track );
    my @playlists = $schema->storage->dbh->selectall_array(
        "SELECT id, name FROM public.readylists WHERE list_audios @> ARRAY[UUID('". $clean_track ."')]"
    );
    print $file_out sprintf "%s\t%s\t%s\n", $track, $playlists[0][0], $playlists[0][1] if (@playlists);
    @playlists = $schema->storage->dbh->selectall_array(
        "SELECT id, name FROM client.playlists WHERE list_audios @> ARRAY[UUID('". $clean_track ."')]"
    );
    print $file_out sprintf "%s\t%s\t%s\n", $track, $playlists[0][0], $playlists[0][1] if (@playlists);
}

=cut


sub is_same {
    my ($adr1, $adr2) = @_;
    
    my @adr1 = clean_address($adr1);
    my @adr2 = clean_address($adr2);
    
    my $sum;
    for my $word1 ( @adr1 ){
        for my $word2 ( @adr2 ){
            # warn "$word1 eq $word2" if $word1 eq $word2;
            $sum++ if $word1 eq $word2;
        }
    }

    my $proc = $sum*100/scalar(@adr1);

    return $proc;

    sub clean_address {
        my $address = shift;
        
        my @thesaurus = qw( г город ул улица д дом );

        $address =~ s/[,.]//g;
        $address = lc($address);

        my @adr = split(/\s/, $address );

        for my $word ( @thesaurus ){
            @adr = grep { $_ ne $word } @adr;
        }

        return @adr;
    }


}

warn "\tDONE\n";