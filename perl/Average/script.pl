#!/usr/local/bin/perl

use uni::perl       qw| :dumper |;

open my $fh, 'grades' or die "Can't open grades: $!";
binmode(STDOUT, ':utf8');

my %grades;
while ( my $line = <$fh> ){
    my ($student, $grade) = split ' ', $line;
    $grades{$student} .= $grade . ' ';
}

close $fh;

foreach my $student ( sort keys %grades ){
    my $scores = 0;
    my $total = 0;
    my @grades = split ' ', $grades{$student};

    for my $grade ( @grades ){
        $total += $grade;
        $scores++;
    }
    my $average = $total / $scores;
    print "$student: $grades{$student}\tСреднее $average\n";
}


exit;