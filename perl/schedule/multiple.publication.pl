#!/usr/local/bin/perl

=head 

    Скрипт для копирования расписания (множественная публикация) на переданные точки
    Входящие параметры: task_id - ID задания из task.items
    В параметрах задания: user_id - пользователь установивший задание на коприрование, filial_key - ключ откуда копируется, list_filial_key - ключи куда копируется

=cut

use lib::abs        qw| ../../lib |;
use uni::perl       qw| :dumper |;
use URI;
use LWP::UserAgent;
use HTTP::Headers;
use JSON::XS        qw| decode_json encode_json |;
use Archive::Zip    qw| :ERROR_CODES :CONSTANTS |;
$Archive::Zip::UNICODE = 1;

use External::Config2;
use FonMix::Schema::MainDB;
use FonMix::Model::Store;

use constant {
    DEBUG               => $ENV{DEBUG} // 0,
    URI_GET_PLAYLIST    => '/api/yellow/base/playlist/item/get',
    SLEEP_TIME          => 1,
};

my $config = External::Config2->new( project => 'fonmix' );

my $tmp_dir;
if ( DEBUG ) {
    $tmp_dir = $ENV{HOME}.'/tmp';
    $config->{host} = $ENV{HOST} if DEBUG == 2;
} else {
    $tmp_dir    = '/spool/tmp';
}

my $schema = FonMix::Schema::MainDB->connect( @{$config->{'Model::MainDB'}->{'connect_info'}} );
my $dbh = $schema->storage->dbh;

my $args = FonMix::Plugin::Utils->args(
    task_id => {
        required => 1,
        type     => 'integer',
    }
);
my $task_id = $args->{task_id};

my $task;
# берём в работу 
eval {
    $schema->txn_do(sub {
        $task = $dbh->selectrow_hashref(
            'SELECT * FROM task.items WHERE id = ? AND status_id = 11 FOR UPDATE NOWAIT', {},
            $task_id,
        );
        die 'Tack "'.$task_id.'" with status_id=11 not found!' unless $task;
        $dbh->do(
            'UPDATE task.items SET status_id = 12 WHERE id = ?', {},
            $task_id,
        );
    });
};
if ($@) {
    die;
}

# получаем входящие параметры
my $params = eval { decode_json( $task->{json_params} ) };
if ($@){
    die fail("Error to parse task input params '$task_id': $@");
}

# required params
for ( qw( filial_key user_id list_filial_key ) ){
    die fail("Param '$_' is required") unless exists $params->{$_};
}

die fail("Must be at least one filial_key") unless @{ $params->{list_filial_key} || [] };

my $schedule_item = $dbh->selectrow_hashref(
    'SELECT * FROM schedule.items WHERE filial_key = ?', {},
    $params->{filial_key},
);

die fail("Can't get schedule item on filial_key - '$params->{filial_key}', task_id - '$task_id'")
  unless $schedule_item;

my $schedule = eval { decode_json( $schedule_item->{json_schedule} ) };
if ($@){
    die fail("Error to parse schedule.item json_schedule '$task_id': $@");
}

my $result_json = {
    success => 1,
    data    => {
        schedule    => $schedule,
        playlists   => [],
    },
};

my %list_related;
while ( my ( $day, $blocks ) = each( %{ $schedule } ) ) {
    foreach my $block ( @{ $blocks } ) {
        foreach my $playlist_id ( @{ $block->{id} } ){
            next if exists $list_related{$playlist_id};
            my $json_playlist = get_meta_for_playlist_from_fonmix( $playlist_id );
            if ( $json_playlist && ref $json_playlist eq 'HASH' ) {
                push @{ $result_json->{data}->{playlists} }, $json_playlist;
                $list_related{$playlist_id} = 1;
            } else {
                my $message = "Не удалось получить информацию о подборке '$playlist_id'".( $json_playlist ? ": $json_playlist" : '' );
                die fail( $message );
            }
        }
    }
}

my $file_name = "multiple.schedule.task_id_${task_id}.time_".time;
my $zip_name = "$tmp_dir/$file_name.zip";
my $zip = Archive::Zip->new();
$zip->addString( encode_json $result_json, $file_name . ".json" );

unless ( $zip->writeToFileNamed( "$zip_name" ) == AZ_OK ) {
    die fail( "Can't create zip archive - $zip_name" );
}

# загружаем созданный файл в хранилище
my $model_store = FonMix::Model::Store->new();
my $store_source_data = $model_store->add_item_with_file( { file => $zip_name } );

unlink $zip_name;

unless ( ref $store_source_data eq 'HASH' && $store_source_data->{uri} ) {
    die fail( 'Не удалось сохранить файл с обновлением на Store' );
}

my $command_values = encode_json( { uri => $store_source_data->{uri} } );
my $action = $dbh->selectrow_hashref(
    'SELECT id FROM device.actions WHERE alias = \'update_audio_schedule\'', {},
);

# Обновляем расписание на переданные точки
foreach my $filial_key ( @{ $params->{list_filial_key} || [] } ){
    $schema->resultset('Schedule::Item')->search({ filial_key => $filial_key })->first->update({
        json_schedule => $schedule_item->{json_schedule},
        modify_user_id => $params->{user_id}
    });
}

my $txn_guard = $schema->txn_scope_guard();
foreach my $filial_key ( @{ $params->{list_filial_key} || [] } ){

    # Отменяем предыдущие задания на обновление, если плеер их еще не выполнил
    my $prev_action = $dbh->selectrow_hashref(
        'SELECT * FROM device.action_queue WHERE key = ? AND action_id = ? AND status = 0', {},
        $filial_key,
        $action->{id},
    );

    if ( defined $prev_action ){
        $dbh->do(
            'UPDATE device.action_queue SET status = -1, errmess = ? WHERE key = ? AND action_id = ? AND status = 0', {},
            'Отменено',
            $schedule_item->{filial_key},
            $action->{id},
        );    
    }

    my $action_queue = $dbh->selectrow_hashref(
        'INSERT INTO device.action_queue ( action_id, key, user_id, values ) VALUES ( ?, ?, ?, ? ) RETURNING *', {},
        $action->{id},
        $filial_key,
        $params->{user_id},
        $command_values,
    );

    unless ( $action_queue->{id} ){
        die fail( 'Не удалось поставить команду плееру на обновление' );
    };

    my $publication = $dbh->do(
        'INSERT INTO schedule.publications (process_status, process_message, user_id, schedule_id, json_schedule, device_action_queue_id, note) VALUES ( ?, ?, ?, ?, ?, ?, ? ) RETURNING *', {},
        13, # выполнена
        'OK',
        $params->{user_id},
        $schedule_item->{id},
        $schedule_item->{json_schedule},
        $action_queue->{id},
        "Было скопированно с ключа: $params->{filial_key}",
    );

    # Чтобы базу не нагружать при копировании 1000+ записей
    sleep SLEEP_TIME;
}
$txn_guard->commit();

$schema->storage->dbh->do('UPDATE task.items SET status_id = 13, message = ?  WHERE id = ?', {},
    'OK', $task_id,
);

exit;

# получение меты по подборке с сервера по API
sub get_meta_for_playlist_from_fonmix {
    my $playlist_id = shift;
    my $json_playlist;

    my %params = (
        'place' => '25',
        'with'  => 'list_content_meta',
        'id'    => $playlist_id,
    );
    my $url = URI->new( 'http://' . $config->{host} . URI_GET_PLAYLIST );
    $url->query_form( %params );

    $json_playlist = _get_from_fonmix( $url );

    return $json_playlist->{error} unless $json_playlist->{success};
    return $json_playlist->{data}->{item};
}

# выполняет запрос на fonmix
sub _get_from_fonmix {
    my $url = shift;

    my $ua = LWP::UserAgent->new();
    my $h  = HTTP::Headers->new();

    $h->header( 'X-Secret-Key' => $config->{FonMix}{secret_key} );
    $ua->default_headers( $h );

    my $response = $ua->get( $url );
    my $res_hash = eval { decode_json( $response->decoded_content ) };
    if ( $@ ) {
        fail( $@ . "\n" . $response->decoded_content );
        return {};
    }

    return $res_hash;
}

sub fail {
    my $message = shift;

    $schema->storage->dbh->do(
        'UPDATE task.items SET status_id = -13, message = ?  WHERE id = ?',
        {}, $message, $task_id, );

    return $message;
}
