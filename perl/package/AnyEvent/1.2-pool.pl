#!/usr/bin/perl
use uni::perl qw/:dumper/;
use AnyEvent::Fork::Pool;
use AnyEvent::Fork;

# Кол-во ядер в системе, по умолчанию 1
my $cpus = AnyEvent::Fork::Pool::ncpu 1;

# Названия модуля
my $mod = 'Worker';

# Название функции
my $sub = 'work';

# Инит форка
my $fork = AnyEvent::Fork->new;

# Инит пула
my $pool = $fork->require($mod)->AnyEvent::Fork::Pool::run(
    "${mod}::$sub",             # Worker::work - рабочая функия ворека
    init    => "${mod}::init",  # Worker::init - инит воркера
    max     => $cpus,           # Макс. кол-во воркеров
    idle    => 0,               # Кол-во воркеров при простое
    load    => 1,               # Размер очереди воркеров
);

# Постановка задач
for my $str (qw( test hello )){
    $pool->($str, sub {
        say "result: @_";
    });
}

AnyEvent->condvar->recv;

exit;