#!/usr/bin/env perl

use MyPackage;

print 1;

my $test = MyPackage->new;

=head

sub new {
    my $self = shift;
    my $caller = caller;

    my ( $class, %params ) = @_;
    my $self = {};
    bless $self, ;
    return ;
}

sub test1 { 1 };
sub test2 { 2 };
sub test3 { 3 };
sub test4 { 4 };

sub import {
    my $self = shift;
    my $caller = caller;

    for my $func ( qw( pod_file_ok all_pod_files all_pod_files_ok ) ) {
        no strict 'refs';
        *{$caller."::".$func} = \&$func;
    }

    $Test->exported_to($caller);
    $Test->plan(@_);
}


