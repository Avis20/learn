package WebApp::Controller::Cabinet;
use Moose;
use namespace::autoclean;
use uni::perl qw/ :dumper /;

BEGIN { extends 'Catalyst::Controller' }

__PACKAGE__->config(namespace => 'cabinet');

sub base :Chained('') :PathPart('cabinet') :CaptureArgs(0) {
    my ( $self, $c ) = @_;
}

sub index :Chained('base') :PathPart('') :Args(0) {
    my ( $self, $c ) = @_;

    my $body = qq~
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Document</title>
        </head>
        <body>
            <h2>Hello</h2>
        </body>
        </html>
    ~;

    $c->res->body($body);
}

1;