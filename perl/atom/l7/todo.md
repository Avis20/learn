

## Замыкания

<pre><code class="perl">

</code></pre>

## Any::Event

### AE::io
### AE::timer

<pre><code class="perl">
    state $test = 0; - что это?
</code></pre>

### AE::signal - обработка сигналов
### AE::idel - когда нечего делать

### AE::cv (condvar) - следит что цикл сошелся в одной точке
### AE::cv (begin/end)

### Пять паралельных задач

### Async::Chain

### Coro - зеленые треды

<pre><code class="perl">use Coro;

async {
    say 2;
    cede;
    say 4;
}

say 1;
cede;
say 3;
cede;
</code></pre>

### ДЗ1

сервер на порту

AnyEvent::Memchache - клиент

### ДЗ2

прокси 


TODO: tyhash - тайные значения
