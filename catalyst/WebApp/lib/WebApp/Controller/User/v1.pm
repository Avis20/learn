package WebApp::Controller::User::v1;
use Moose;
use namespace::autoclean;
use uni::perl qw| :dumper |;

use File::Slurp ':all';

BEGIN { extends 'Catalyst::Controller' }

__PACKAGE__->config(namespace => 'user/v1');

sub base :Chained('/user/base') :PathPart('v1') :CaptureArgs(0) {
    my ( $self, $c ) = @_;
    $c->stash->{db_file} = $c->config->{home}.'/data/data_base_file.txt';
}

sub index :Chained('base') :PathPart('') :Args(0) {
    my ( $self, $c ) = @_;

    my $body = qq~
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Document</title>
        </head>
        <body>
            <h2>Person list</h2>
            <a href="/user/v1/add/">Add new user</a>
    ~;

    my @rows = read_file( $c->stash->{db_file} ) if -f $c->stash->{db_file};
    chomp @rows;

    if ( scalar @rows > 0 ){

        $body .= qq~
            <table border="1" style="border-collapse: collapse">
            <tr>
                <th>ID</th>
                <th>name - email</th>
                <th>delete</th>
            </tr>
        ~;

        foreach my $row ( @rows ){
            my ( $id, $name, $email ) = split /\|/, $row;
            $body .= qq~
                <tr>
                    <td>$id</td>
                    <td><a href="/user/v1/edit/$id">$name - $email</a></td>
                    <td><a href="/user/v1/delete/$id">X</a></td>
                </tr>
            ~;
        }

        $body .= qq~ </table> ~;
    };

    $body .= qq~ </body></html> ~;

    $c->res->body($body);

}

sub add :Chained('base') :PathPart('add') :Args(0) {
    my ($self, $c) = @_;

    my %params = %{ $c->req->params };

    if ( $params{submit} ){

        my @rows = read_file( $c->stash->{db_file} ) if -f $c->stash->{db_file};
        chomp @rows;

        my $id = ( $rows[-1] =~ /^(\d+)/ ? $1 + 1 : 1 );

        write_file($c->stash->{db_file}, { append => 1 },
            "$id|$params{name}|$params{email}\n"
        );

        $c->res->redirect('/user/v1/');

    } else {

        my $body = qq~
            <!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <title>Document</title>
            </head>
            <body>
                <form action="/user/v1/add/">
                    <table>
                    <tr>
                        <th>name</th>
                        <th>email</th>
                    </tr>
                    <tr>
                        <td>
                            <label for="name"></label>
                            <input type="text" name="name" />
                        </td>
                        <td>
                            <label for="email"></label>
                            <input type="text" name="email" />
                        </td>
                        <td>
                            <input type="submit" name="submit" value="Save" />
                        </td>
                    </tr>
                </form>
            </body>
            </html>
        ~;

        $c->res->body($body);
    }
}

sub edit :Chained('base') :PathPart('edit') :Args(1) {
    my ($self, $c, $id) = @_;

    my %params = %{ $c->req->params };

    if ( $params{submit} ){

        edit_file_lines { s/^$id\|.*/$id|$params{name}|$params{email}/ } $c->stash->{db_file};
        $c->res->redirect('/user/v1/');

    } else {

        my @rows = read_file( $c->stash->{db_file} ) if -f $c->stash->{db_file};
        chomp @rows;

        my ( $file_id, $name, $email );
        foreach my $row ( @rows ){
            my ( $find_id ) = $row =~ /^(\d+)/;
            if ( $id == $find_id ){
                ( $file_id, $name, $email ) = split /\|/, $row;
            }
        }

        my $body = qq~
            <!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <title>Document</title>
            </head>
            <body>
                <form action="/user/v1/edit/$id">
                    <table>
                    <tr>
                        <th>name</th>
                        <th>email</th>
                    </tr>
                    <tr>
                        <td>
                            <label for="name"></label>
                            <input type="text" name="name" value="$name"/>
                        </td>
                        <td>
                            <label for="email"></label>
                            <input type="text" name="email" value="$email"/>
                        </td>
                        <td>
                            <input type="submit" name="submit" value="Save" />
                        </td>
                    </tr>
                </form>
            </body>
            </html>
        ~;

        $c->res->body($body);
    }

}

sub delete :Chained('base') :PathPart('delete') :Args(1) {
    my ($self, $c, $id) = @_;

    edit_file_lines { s/^$id\|.*\n// } $c->stash->{db_file};
    $c->res->redirect('/user/v1/');
}

1;
