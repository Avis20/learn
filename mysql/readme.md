

## Links ##
* http://www.mysql.ru/docs/man/Tutorial.html

# Создание БД #

## show databases - Показать список БД ##

```
#!bash

show databases;

+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
4 rows in set (0,00 sec)
```

## create database - Создать БД ##

```
#!bash

show databases;

Query OK, 1 row affected (0,00 sec)
```

## use - выбрать для дальнейшей работы ##

```
#!bash

use pets;

```

# Таблицы #

## show tables - список таблиц ##

```
#!bash

show tables;

Empty set (0,00 sec)
```

## create table - создание таблицы ##

```
#!bash

create table pet (name varchar(20), owner varchar(20), type varchar(20), sex char(1), birth date, death date );

```

## describe - структура таблицы ##

```
#!bash

describe pet;

+-------+-------------+------+-----+---------+-------+
| Field | Type        | Null | Key | Default | Extra |
+-------+-------------+------+-----+---------+-------+
| name  | varchar(20) | YES  |     | NULL    |       |
| owner | varchar(20) | YES  |     | NULL    |       |
| type  | varchar(20) | YES  |     | NULL    |       |
| sex   | char(1)     | YES  |     | NULL    |       |
| birth | date        | YES  |     | NULL    |       |
| death | date        | YES  |     | NULL    |       |
+-------+-------------+------+-----+---------+-------+
6 rows in set (0,03 sec)

```

# Вставка данных #

## load data local - загрузить из файла ##

```
#!bash

load data local infile '/home/avis/develop/learn/mysql/pet.txt' into table pet;

```

## insert - вставка одиночных данных ##

```
#!bash

insert into pet values ('puffball', 'Diane', 'hamster', 'f', '2017-03-30', null);

```

# Выполнение команд #

## Выполнить команды из файла ##

```
#!bash

mysql < file.sql

```
