package fastpaste;

use Dancer2;
use Dancer2::Plugin::Database;

use uni::perl   qw| :dumper |;
use Digest::CRC qw| crc64 |;
use HTML::Entities;

our $VERSION = '0.1';
my $upload_dir = 'paste';

get '/' => sub {
    template 'index';
};

post '/' => sub {
    my $text = params->{textpaste};
    my $title = params->{title} || '';
    my $expire = params->{expire};

    my @errors;

    push @errors, 'Empty text' unless $text;
    push @errors, 'Text to large' if length $text > 10240;
    push @errors, 'Expire bad format' if $expire =~ /\D/ && $expire < 0 && $expire > 3600 * 24 * 365;
    
    if ( @errors ){
        $title = encode_entities($title, '<>&"');
        $text = encode_entities($text, '<>&"');
        return template 'index' => {
            err => \@errors,
            title => $title,
            expire => $expire,
            text => $text,
        };
    }

    my $create_time = time();
    my $expire_time = $expire ? $create_time + $expire : undef;

    my $sth = database->prepare('
        INSERT INTO paste (id, create_time, expire_time, title)
        VALUES (cast(? as signed), from_unixtime(?), from_unixtime(?), ?)');

    my $id;
    my $try_count = 10;
    while (!$id or -f get_upload_dir . $id){
        database->do('DELETE FROM paste WHERE id = (cast(? as signed)', {}, [$id]) if $id;
        unless ( --$try_count ){
            $id = undef;
            last;
        }
        $id = crc64($text . $create_time . $id);
        $id = undef unless( $sth->execute($id, $create_time, $expire_time, $title) );
    }

    die "Try latter" unless $id;

    open my $fh, '>', get_upload_dir() . $id or die "Internal error! $!";
    print $fh $text;
    close $fh;
    redirect '/' . unpack 'H*', pack 'Q', $id;

};

get qr|^/([a-f0-9]{16})$| => sub {
    my ( $id ) = splat;
    $id = unpack 'Q', pack 'H*', $id;
    my $sth = database->prepare('
        SELECT cast(id as signed), title, create_time, unix_timestamp(expire_time)
        FROM paste WHERE id = cast(? as signed)'
    );

    unless ( $sth->execute($id) ){
        response->status(404);
        return template 'index' => { err => [ 'Fast paste not found' ] };
    }

    my $db_res = $sth->fetchrow_hashref();
    if ( $db_res->{expire_time} and $db_res->{expire_time} < time() ){
        delete_paste($id);
        response->status(404);
        return template 'index' => { err => ['Fast paste expired'] };
    }

    my $fh;
    unless ( open $fh, '<:utf8', get_upload_dir() . $id ){
        delete_paste($id);
        response->status(404);
        return template 'index' => { err => ['Fast paste not found'] };
    };

    my @text = <$fh>;
    close $fh;
    for (@text) {
        $_ = encode_entities($_, '<>&"');
    }

    return template 'paste_show.tt' => {
        id      => $id,
        text    => \@text,
        title   => $db_res->{title},
        raw     => join '', @text,
    };

};

hook 'before_template_render' => sub {
    my $tokens = shift;
    my $last_paste = database->selectall_arrayref('
        SELECT cast(id as signed) AS id, title, create_time, unix_timestamp(expire_time)
        FROM paste
        WHERE expire_time > current_timestamp
        ORDER BY create_time DESC
        LIMIT 10
    ', { Slice => {} });

    for (@$last_paste){
        $_->{title} = encode_entities($_->{title}, '<>&"');
        $_->{id} = unpack 'H*', pack 'Q', $_->{id};
    }

    $tokens->{last_paste} = $last_paste;
};

sub get_upload_dir {
    return config->{appdir} . "/$upload_dir/";
}

sub delete_paste {
    my $id = shift;
    database->do('DELETE FROM paste WHERE id = cast(? as signed)', {}, $id);
    unlink get_upload_dir() . $id;
}

true;
