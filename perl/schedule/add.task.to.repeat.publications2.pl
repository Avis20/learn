#!/usr/local/bin/perl

=head
    Скрипт проверяет публикации которые нужно обновить и создает задание в task.items на обновление
=cut

use lib::abs        qw| ../../lib |;
use uni::perl       qw| :dumper |;
use JSON::XS        qw| decode_json encode_json |;

use External::Config2;
use FonMix::Schema::MainDB;

my $config = External::Config2->new( project => 'fonmix' );
my $schema = FonMix::Schema::MainDB->connect( @{$config->{'Model::MainDB'}->{'connect_info'}} );
my $dbh = $schema->storage->dbh;

my $publications = $dbh->selectall_hashref(
    'SELECT id, list_update_playlist, device_action_queue_id, task_id
     FROM schedule.publications WHERE to_update IS TRUE', 'id'
);

foreach my $publish_id ( keys %{ $publications || {} } ){
    my $publication = $publications->{$publish_id};

    my $params;
    if ( $publication->{task_id} ){

    } else {
        $params = encode_json({
            'list_publication'   => [$publish_id],
            # 'list_playlist'     => $publication->{'list_update_playlist'},
        });
    }

    my $task = $dbh->selectrow_hashref( 
        'INSERT INTO task.items ( script_id, status_id, user_id, json_params ) VALUES ( ?, ?, ?, ? ) RETURNING *', {},
        2,  # repeat.publication
        11, # новая
        25, # системный пользователь
        $params,
    );
    
    die "Can't create task!" unless defined $task;

    # Отменяем предыдущие задания на обновление, если плеер их еще не выполнил
    if ( defined $publication->{device_action_queue_id} ){
        $dbh->do(
            'UPDATE device.action_queue SET status = -1, errmess = ? WHERE id = ? AND status = 0', {},
            'Отменено',
            $publication->{device_action_queue_id},
        );
    }

    $dbh->do(
        'UPDATE schedule.publications SET to_update = FALSE WHERE id = ?', {},
        $publish_id,
    );
}

exit;
