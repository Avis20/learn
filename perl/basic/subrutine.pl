#!/usr/bin/perl
use uni::perl qw/:dumper/;
use feature 'signatures';

warn foo(1,2);

sub foo ($left, $right) {
    return $left + $right;
}

exit;