#!/usr/bin/perl

use uni::perl qw/:dumper/;
use AnyEvent;
use Gtk2 -init;

#####################################
# Создаем окно и надпись
my $window = Gtk2::Window->new("toplevel");
$window->add( my $label = Gtk2::Label->new("hi") );

$window->show_all;
#####################################
# Генерация события

$| = 1;
print "Enter your name>";

my $name_read = AnyEvent->condvar;

my $wait_to_input = AnyEvent->io (
    fh      => \*STDIN,     # какой дескриптор файла проверять
    poll    => "r",         # какое событие ждать("r"ead - чтение)
    cb      => sub {
        # Изменяем надпись в окне
        $label->set_text(scalar <STDIN>);
        print "Enter another name>";
    },
);

say "\nDo something else";

main Gtk2;